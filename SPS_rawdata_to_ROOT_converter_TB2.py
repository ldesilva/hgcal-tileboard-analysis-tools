"""
LED DATA CONVERSION SCRIPT

Converts individual ROOT files into a single ROOT file for further processing.

Only can be used for data taken with TB-tester DAQ system
CANNOT BE USED WITH KCU105 DAQ DATA

----------------
TO EXECUTE

python3 SPS_rawdata_to_ROOT_converter_TB2.py <path/to/ROOT files>

----------------
RETURNS

root_SPS_***_ref0.root, root_SPS_***_refSeparated.root and root_SPS_***_refcorrected.root 


root_SPS_***_refcorrected.root 

    Contains a root file with the following columns (key="data")

                ["channel","ConvGain","BoardConfig","OverVolt","extLED",
                 "offset","phase","refinv","bxID","adc","tot","toa"]


    channel     = HGCROC channel number according to the tileboard floorplan. 
                  Not to confuse with the raw HGCROC channels as that cordinate system is different
    ConvGain    = Conveyor Gain. Default is 12. Value taken from run_map. 
    OverVolt    = Over voltage according to file name (/run_OV*V)
    
    extLED      = LED bias voltage supplied
    offset      = L1A offset set (in order to sample pulse)
    phase       = phase offset setting (16 phases = 1 bunch crossing = 25 ns delay) 

    adc         = ADC measured by HGCROC
    toa         = time of arrival in ADC
    tot         = time over threshold in ADC
    
    refinv      = ref_inv_dac parameter set. 
                  refinv = 0 : Channel's nominal/default value  
                  refinv = 1 : Channel's nominal/default value + 1
                  refinv = 2 : Channel's nominal/default value + 2

                The different refinv values are used to reduce the effect of the differential non-linearity (DNL) 
                present in the HGCROC. 
                Each increment of refinv changes the pedestal by roughly 3 ADC bins. 
                The DNL is worst at every 2nd and 4th bin.
                Therefore by taking three data sets, subtracting the pedestal offset of 3 and 6 ADCs for refinv=1 and refinv=2
                respectively and then adding the three datasets together, one can minimize the DNL effect on the ADC. 
                
                NOTE: work is on-going to use a floating average method using just one of the datsets. 

"""

import pandas as pd
import numpy as np
from glob import glob
import os,sys

import ROOT

import matplotlib as mpl
import matplotlib.pyplot as plt

import seaborn as sns

# set font size
font = {'size'   : 20}
mpl.rc('font', **font)
# mpl.rcParams['text.usetex'] = True

import uproot

def main(base_dir):
    # read dataframes
    
    fnames = glob(base_dir + "/*_extLED*/run_*/injection_run0.root")
    print("Found %i files" % len(fnames))
    
    name_dir = base_dir.split("/")[-1]
    if name_dir=="": name_dir = base_dir.split("/")[-2]
    print("name_dir:",name_dir)
    
    dfs = pd.DataFrame()

    for fname in sorted(fnames):
        try:
            print(fname)
            file = uproot.open(fname)
            tree = file["unpacker_data"]["hgcroc"]
            #     df = tree.arrays(library="pd")     # for LCG > 101
            df = tree.pandas.df()                    # for LCG =  99

            # Add the information on overvoltage, configuration, LED bias voltage, refinv etc to the dataframe
            
            if fname.find("_OV") >0 : 
                start = int(fname.find("_OV") + 3)
                df["OverVolt"] = int(fname[start:start+1])
            else: print("no OverVolt")

            if fname.find("Config") >0 : 
                df["BoardConfig"] = int(fname[fname.find("Config")+6:fname.find("Config")+7])
            elif fname.find("config") >0 : 
                df["BoardConfig"] = int(fname[fname.find("config")+6:fname.find("config")+7])
            else : df["BoardConfig"] = 1
#             df = df.astype({'BoardConfig': 'int32'}).dtypes
#             print(df["BoardConfig"])
            
            if fname.find("phase") >0 : 
                ph = fname[fname.find("phase")+5:fname.find("phase")+7]
                if (ph[1]=="_" or ph[1]=="/"): ph = ph[0]
                df["phase"] = int(ph)
        #     if fname.find("phase_9") >0 : df["phase"] = 9
        #     elif fname.find("phase_4") >0 : df["phase"] = 4

            if fname.find("extLED") >0 : 
                start = int(fname.find("extLED") + 6)
                end = int(fname.find("mV"))
        #         print("start")
        #         print(fname[start:end])
                df["LED"] = int(fname[start:end])
            else: 
                print("no LED parameter. LED set to 6000 mV")
                df["LED"] = 6000

            if fname.find("_Temp") >0 : 
                start = fname.find("_Temp") + 5
                end = fname.find("p0/")
                df["temperature"] = float(fname[start:end])
            else: df["temperature"] = 22.7

            if fname.find("ConvGain") >0 : 
                cg = fname[fname.find("ConvGain")+8:fname.find("ConvGain")+10]
                if cg[1] == "_": cg = cg[0]
                df["ConvGain"] = int(cg)
            else: 
                print("no ConvGain parameter. ConvGain set to 12")
                df["ConvGain"] = 12

            if fname.find("BX") >0 : 
                of = fname[fname.find("BX")+2:fname.find("BX")+4]
                df["offset"] = int(of)

            if fname.find("refinv") >0 : 
                of = fname[fname.find("refinv")+6:fname.find("refinv")+7]
                df["refinv"] = int(of)

            elif fname.find("ref") >0 : 
                of = fname[fname.find("ref")+3:fname.find("ref")+4]
                df["refinv"] = int(of)

            dfs=dfs.append(df)
            
        except KeyError:
            pass

    
    
    dfs_rdf = {key: dfs[key].values for key in dfs.columns}
    
    # save data set as ***_refSeparated.root
    rdfs = ROOT.RDF.MakeNumpyDataFrame(dfs_rdf)
    rdfs.Snapshot("data", base_dir+"/root_"+name_dir+"_refSeparated.root")
    print("Saved as " + base_dir+"/root_"+name_dir+"_refSeparated.root")

    # dfs.to_hdf(base_dir+"/root_"+name_dir+"_ref0.h5", key = "data")
    
    # save refinv=0 data as ***_ref0.root
    dfs0 = dfs[dfs.refinv == 0]
    dfs0_rdf = {key: dfs0[key].values for key in dfs0.columns}

    rdfs0 = ROOT.RDF.MakeNumpyDataFrame(dfs0_rdf)
    rdfs0.Snapshot("data", base_dir+"/root_"+name_dir+"_ref0.root")
    print("Saved as " + base_dir+"/root_"+name_dir+"_ref0.root")

    df_chans = dfs
    print(dfs)
    
    # do the refinv correction : refinv=1 => adc=adc-3     and     refinv=2 => adc=adc-6
    df_chans['adc'] = np.where(df_chans['refinv'] == 2, df_chans['adc']-6, df_chans['adc'])
    df_chans['adc'] = np.where(df_chans['refinv'] == 1, df_chans['adc']-3, df_chans['adc'])

    # print(dfs)
    print("ref merge complete")

    df_chans_rdf = {key: df_chans[key].values for key in df_chans.columns}
    
    # save refinv corrected dataset as ***_refcorrected.root
    rdf_chans = ROOT.RDF.MakeNumpyDataFrame(df_chans_rdf)
    rdf_chans.Snapshot("data", base_dir+"/root_"+name_dir+"_OV"+str(OV)+"_refcorrected.root")
    print("Saved as " + base_dir+"/root_"+name_dir+"_OV"+str(OV)+"_refcorrected.root")

    # df_chans.to_hdf(base_dir+"/root_"+name_dir+"_refcorrected.h5", key = "data")



if __name__ == "__main__":
    if len(sys.argv) == 2:
        fname = sys.argv[1]
        print(fname)
        main(fname)

    else:
        print("No argument given")
        print(len(sys.argv))
    