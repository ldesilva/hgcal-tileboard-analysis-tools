import pandas as pd
import numpy as np
from glob import glob
import os, sys

import matplotlib as mpl
import matplotlib.pyplot as plt

from langaus import LanGausFit
import langaus
import ROOT

from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.optimize import OptimizeWarning
import scipy.special as sp

import math 

import warnings
warnings.simplefilter("error", OptimizeWarning)

# set font size
font = {'size'   : 20}
mpl.rc('font', **font)

outputHDF5file = "/df_TestBeam.h5"

run_fl = "/*/df_summary_*.h5"

Vindac = 1.600                                # TB1 voltage for inputdac=31. ( OV = Vbias - (Vindac + Vbd) )
Vadapter_dif = 1.67
Vbd_nominal = 38.0
Vindac_err = 0.010
Temp_grad = 0.035   # in 
dict_OV_map    = {"TB1.2_1":{55:38.22,56:38.08,57:37.75,58:38.11,59:37.82,60:37.89,61:37.88,62:37.94}}

def main(base_dir,Tileboard):
    df_width = pd.DataFrame(columns=["channel","Config","Tileboard","ConvGain","OverVolt","SupVolt","SupVolt_error",
                                     "ADC","ADC_error","LandauWidth","LandauWidth_error","GausWidth","GausWidth_error",
                                     "TotalWidth","TotalWidth_error","Ped_RMS","RMS_error"])   

    file_lst = 0
    run_dir = base_dir + run_fl 
    fnames = glob(run_dir)
    print("Found %i data files" % len(fnames))
    if len(fnames)>0:
        for fname in fnames:
            p_dif = pd.read_hdf(fname, key = "data")
            chan = p_dif.channel.unique()[0]

            for gain in p_dif.ConvGain.unique():
                df_psel = p_dif[p_dif.ConvGain==gain]
            df_psel = df_psel.reset_index()
            
            if fname.find("config") > 0:
                config = int(fname[fname.find("config")+6:fname.find("config")+7])
            elif fname.find("Config") > 0 :
                config = int(fname[fname.find("Config")+6:fname.find("Config")+7])
            else:
                config = 1

            strt_OV = run_dir.find("_OV")+3
            fin_OV  = run_dir.find("_OV")+4
#             print(run_dir[strt_OV:fin_OV])

            OV = float(run_dir[strt_OV:fin_OV].replace('p', '.'))
    
            SupVolt = OV + Vadapter_dif + Vbd_nominal - Vindac - dict_OV_map[Tileboard][chan]
            SupVolt_err = Vindac_err
            
            print("OV:",OV,"V")
            print("Vadapter Voltage:",OV + Vadapter_dif + Vbd_nominal,"V")
            print("Vindac:-",Vindac,"V")
            print("Vbd:",dict_OV_map[Tileboard][chan],"V")
            print("SupVolt:",SupVolt,"V")
            lolim = 30
            ConvGain = p_dif.ConvGain.unique()[0]
            if   ConvGain <= 4 :   uplim = 300
            elif ConvGain <= 12:   uplim = 400
            elif ConvGain <= 15:   uplim = 500

            if OV >= 3.0 :  
    #             lolim = lolim*2
                uplim += uplim
            if OV >= 5.0 :  
    #             lolim += lolim*4 
                uplim += uplim*3

            nbins = int(len(df_psel)/250)
    #             nbins = 300

            if uplim >1100: uplim = 1100

            canvas1 = ROOT.TCanvas("canvas1","canvas1",800, 600)
            canvas1.SetFillColor(0)
            canvas1.SetGrid()
            canvas1.GetFrame().SetFillColor(2)
            canvas1.GetFrame().SetBorderSize(12)
    #             histLangaus = ROOT.TH1D("dataLangaus", "dataLangaus", nbins, 100,df_psel.ADC.max())
            histLangaus = ROOT.TH1D("dataLangaus", "dataLangaus", nbins, -101,uplim+10)
            histLangaus.SetAxisRange(-100,400)
            histLangaus.SetLineColor(1)
            for element in range(len(df_psel)):
                histLangaus.Fill(df_psel[df_psel.index==element].ADC)

            histLangaus.SetAxisRange(lolim,uplim,"X")
            yuplim = histLangaus.GetBinContent(histLangaus.GetMaximumBin());
            yuplim_val = histLangaus.GetXaxis().GetBinCenter(histLangaus.GetMaximumBin());
    #             print(int(lolim),uplim)
    #             print(yuplim,yuplim_val)


            histLangaus.SetAxisRange(-10,yuplim,"Y")

            histLangaus.GetXaxis().SetTitle("Reconstructed Amplitudes [ADC]")
            histLangaus.GetYaxis().SetTitle("No. of entries")
            histLangaus.GetXaxis().SetLabelSize(0.05);
            histLangaus.GetXaxis().SetTitleSize(0.05);
            histLangaus.GetXaxis().SetNdivisions(505);
            histLangaus.GetXaxis().SetTitleOffset(1.0);
            histLangaus.GetYaxis().SetTitleOffset(1.0);
            histLangaus.GetYaxis().SetLabelSize(0.05);
            histLangaus.GetYaxis().SetTitleSize(0.05);

            fit = LanGausFit()
            func = fit.fit(histLangaus,fitrange=(yuplim_val*0.5,yuplim_val*1.5))
#             func = fit.fit(histLangaus,fitrange=(lolim,uplim-50))

            wdth        = func.GetParameter(0)
            wdth_err    = func.GetParError(0)
            mpv         = func.GetParameter(1)
            mpv_err     = func.GetParError(1)
            norm        = func.GetParameter(2)
            norm_err    = func.GetParError(2)
            mu          = func.GetParameter(3)
            mu_err      = func.GetParError(3)
            
            ped_std = 2.0
            ped_std_err = ped_std*0.01
            
            mpv_WOped      = func.GetMaximumX()
            mpv_WOped_err  = (((mpv_err/mpv)*(mpv_WOped))**2 + ped_std_err**2)**0.5            
            
            snr = mpv_WOped/ped_std

            error = ((wdth)**2 + (mu)**2)**(1/2)
            error_err = np.sqrt((wdth**2 * wdth_err**2 + mu**2 * mu_err**2)/(wdth**2 + mu**2))
            chisq = func.GetChisquare()/func.GetNDF()
            
            df_width = df_width.append(pd.DataFrame([[int(chan),config,Tileboard,gain,OV,SupVolt,SupVolt_err,
                                                      mpv_WOped,mpv_WOped_err,wdth,wdth_err,mu,mu_err,
                                                      error,error_err,ped_std,ped_std_err]],
                                                      columns=df_width.columns))
            
            #     In this way you find the maximum
            y_max = func.GetMaximum();
            x_max = mpv_WOped


        #     //let's search for the point of half maximum, 
        #     //one will be on the left and the other one on the right
        #     //so the reason for the two call, using he lower and upper bound
        #     //of the x range for the fuction.

            fwhm_left = func.GetX(y_max/2, x_max);
            fwhm_right = func.GetX(y_max/2, x_max, df_psel.ADC.mean()+3*df_psel.ADC.std());
            fwhm = fwhm_right - fwhm_left
            print("FWHM/2.3 = " + str(fwhm/2.3))

            fwhm_left = func.GetX(9*y_max/10, x_max);
            fwhm_right = func.GetX(9*y_max/10, x_max, df_psel.ADC.mean()+3*df_psel.ADC.std());
            fwhm = fwhm_right - fwhm_left
            print("FW90%M = " + str(fwhm))

            histLangaus.SetAxisRange(-10,uplim,"X")
            histLangaus.Draw()   
            func.Draw("same") 

            ROOT.gStyle.SetOptTitle(0)
            ROOT.gStyle.SetOptStat(0)
            l1=ROOT.TLegend(0.2,0.60,0.88,0.88)
            l1.SetFillStyle(0)

            l1.SetHeader("For Ch:{} at ConvGain:{} and Overvoltage:{}+/-{} V".format(
                chan,ConvGain,np.round(SupVolt,2),np.round(SupVolt_err,2)),"") # option "C" allows to center the header
            l1.SetTextAlign(33)
            l1.AddEntry(0,"no. of events = {}".format(len(df_psel),""))
            l1.AddEntry(0,"fit maximum = ({}+/-{}) ADC".format(round(mpv_WOped,2),round(mpv_WOped_err,2)),"")
            l1.AddEntry(0,"fit width = ({}+/-{}) ADC".format(round(error,2),round(error_err,2)),"")
            l1.AddEntry(0,"Chi/NDF = {}".format(round(chisq,2)),"")
    #             l1.AddEntry(0,"RMS = {}".format(round(df_psel.ADC.std(),2)),"l")
            l1.SetMargin(0.1);
            l1.SetTextSize(0.04);
            l1.SetTextFont(42);
            l1.SetBorderSize(0);
            l1.Draw()
            

            print("fit maxima for Ch:{} ={}+/-{}, ConvGain:{}".format(chan,round(mpv_WOped,2),round(mpv_WOped_err,2),ConvGain))
            print("fit LandauWidth for Ch:{} ={}+/-{}, ConvGain:{}".format(chan,round(wdth,2),round(wdth_err,2),ConvGain))
            print("fit sigma for Ch:{} ={}+/-{}, ConvGain:{}".format(chan,round(mu,2),round(mu_err,2),ConvGain))
            print(len(df_psel))

            plot_dir=base_dir+"/Landau_dist_Config"+str(config)+"/OverVolt_"+ str(int(np.round(OV,0)))+"V"
            print(len(df_psel))

            if not os.path.exists(plot_dir):os.makedirs(plot_dir)
            canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,ConvGain))
            canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}.C'.format(chan,ConvGain))
            
            canvas1.Draw()
            canvas1.Close()
            print("saved in "+plot_dir)  

        file_lst += 1
        # Save dataframe
    #     print("Saving dataframe as " + base_dir + gain_d + "/df_width.h5")
    #     df_width.to_hdf(base_dir + gain_d + "/df_width.h5", key = "data")
    print("Found %i data files" % file_lst)

    print("Saving dataframe as " + base_dir + outputHDF5file)
    df_width.to_hdf(base_dir + outputHDF5file, key = "data")




if __name__ == "__main__":
    if len(sys.argv) == 3:
        base_dir = sys.argv[1]
        Tileboard = sys.argv[2]
        main(base_dir,Tileboard)

    else:
        print("No argument given ("+str(len(sys.argv))+")")
        print(sys.argv)
