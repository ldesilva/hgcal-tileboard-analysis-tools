"""
LIGHT YIELD CALCULATION USING SPS GAIN AND MIP VALUES. 

THIS IS A SKELETON TO HELP KICK-START YOUR DATA ANALYSIS 

Each testbeam will have a file doing the light yield calculation as shown below. 

THIS IS ONLY A GUIDE! NOT AN ACTUAL SCRIPT DESIGNED TO WORK OUT OF THE BOX

"""

import pandas as pd
import numpy as np
from glob import glob
import os

import matplotlib as mpl
import matplotlib.pyplot as plt

import seaborn as sns
import ROOT

# set font size
font = {'size'   : 20}
mpl.rc('font', **font)

mpl.rcParams['figure.figsize'] = [8,6]

map_fname = "tmp_map_TB2.txt"

df_map = pd.read_csv(map_fname, sep = "\t")
#df_map.chan.hist(bins = range(80))
chs = df_map.chan.values
xs = df_map.x.values
ys = df_map.y.values

ch_map_x = dict(zip(chs,xs))
ch_map_y = dict(zip(chs,ys))


# PDE Based Light Yield Correction 
overvoltage15 = np.array([3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5,7.0])
pde15 =  np.array([28.0,30.5,32.5,35.0,36.25,37.75,38.25,38.75,39.0])
m1,m2,c = np.round(np.polyfit(overvoltage15, pde15, 2),2)

base_dir = "/eos/cms/store/group/dpg_hgcal/tb_hgcal/beamtests_TILEBOARD/DESYTB_Feb22"
fname = base_dir + "/beamtestrun/df_FebTestbeam2022_TB2_6nl1a.h5"

df_points_Feb22 = pd.read_hdf(fname)
df_points_Feb22["SupVolt"] = df_points_Feb22["SupVolt"] + (25 - df_points_Feb22["Temperature"])* 0.035                    
df_points_Feb22

df_sps_Feb22 = pd.read_hdf(base_dir+"/root_SPS_FebTB2022_sel.h5")
df_sps_Feb22 = df_sps_Feb22.drop(columns=["SupVolt"])
df_sps_Feb22

df_points_Feb22= pd.merge(df_points_Feb22, df_sps_Feb22, on=['channel','ConvGain','OverVolt','Tileboard'], how='left')
df_points_Feb22[df_points_Feb22.channel==52]

sps_err = 0.08
s  = df_points_Feb22.channel==50
s &= df_points_Feb22.OverVolt==4

df_points_Feb22[s]

df_points_newFeb22 = pd.DataFrame()

df_points = df_points_Feb22
TB= 'TB2'

df_points_newFeb22
# df_points["SupVolt"] = df_points["SupVolt"] + 0.035*(27 - df_points["Temperature"])

for OV in df_points.OverVolt.unique():
    s = df_points.OverVolt == OV
    df_points_BC2_OV = df_points[s]
    
    x  = df_points_BC2_OV["SupVolt"]
    x0  = np.round(OV,0)
    df_points_BC2_OV["ADC_TempCorr"] = df_points_BC2_OV["ADC"]*(m1*x0**2+m2*x0+c)/(m1*x**2+m2*x+c)
    df_points_newFeb22 = df_points_newFeb22.append(df_points_BC2_OV)  
    
df_points_newFeb22 = df_points_newFeb22
df_points_newFeb22["LightYield"] = df_points_newFeb22["ADC_TempCorr"]/df_points_newFeb22["gain"]
df_points_newFeb22["LY_err"] = df_points_newFeb22["LightYield"] * ((df_points_newFeb22["ADC_error"]/df_points_newFeb22["ADC_TempCorr"])**2 + (sps_err/df_points_newFeb22["gain"])**2)**0.5

# df_points_newFeb22["LightYield"] = df_points_newFeb22["ADC"]/df_points_newFeb22["gain"]
# df_points_newFeb22["LY_err"] = df_points_newFeb22["LightYield"] * ((df_points_newFeb22["ADC_error"]/df_points_newFeb22["ADC"])**2 + (sps_err/df_points_newFeb22["gain"])**2)**0.5
print(df_points_newFeb22)

for OV in df_points_newFeb22.OverVolt.unique():
    for config in df_points_newFeb22.Config.unique():
        s  = df_points_newFeb22.OverVolt == OV
        df_points_BC2_OV = df_points_newFeb22[s]

        df_points_BC2_OV["x"] = df_points_BC2_OV.channel.map(ch_map_x)
        df_points_BC2_OV["y"] = df_points_BC2_OV.channel.map(ch_map_y)    

        df_mp_BC_4V = df_points_BC2_OV.pivot_table(columns='x', index='y', values='LightYield', fill_value=0)

        plt.figure(figsize=(15, 8))
        sns.heatmap(df_mp_BC_4V, cmap = "coolwarm",annot_kws={'size':14}, vmin= 10.0, vmax=40.0, linewidths=0.5, annot=True, fmt=".2f")
        plt.title('Light Yield map for ConvGain=12, Corrected to OV='+str(OV)+'V, '+TB +' config '+str(config))

        plot_dir=base_dir+"/MIP_spectra/Gain_plots/"
        if not os.path.exists(plot_dir):os.makedirs(plot_dir)
        plt.savefig(plot_dir+TB + '_LightYield_map_'+str(OV)+'V_Config'+str(config)+'_ConvGain12_corrected.png')  
        print("saved as "+plot_dir + TB + '_LightYield_map_'+str(OV)+'V_Config'+str(config)+'_ConvGain12_corrected.png')
