"""
Data analysis and summary script using TrigTime cut method. 
This script reads the run_map which has the information on each individual data run

TrigTime cut values are taken from run_map

TrigTime cut can only be applied if data was taken using TB-Tester DAQ system
CANNOT BE USED WITH KCU105 DAQ DATA

----------------
TO EXECUTE

python3 plot_beamdata_TrigTime.py <base directory> <Tileboard version> 

<Tileboard version> = "TB3_1" or "TB3_2" or "mini"

----------------
RETURNS

df_TestBeam.h5

    Contains a pandas DataFrame with the following columns (key="data")

                ["channel","Config","Tileboard","ConvGain","OverVolt","SupVolt","SupVolt_error",
                "ADC","ADC_error","LandauWidth","LandauWidth_error","GausWidth","GausWidth_error",
                "TotalWidth","TotalWidth_error","Ped_RMS","RMS_error","Temperature"]


    channel     = HGCROC channel number according to the tileboard floorplan. 
                     NOTE: Currently only data from second half of tileboard is used
                     
                  Not to confuse with the raw HGCROC channels as that cordinate system is different          
    Config      = Tileboard configuration. Taken from run_map. Depends on testbeam. 
    ConvGain    = Conveyor Gain. Default is 12. Value taken from run_map. 
    OverVolt    = Over voltage according to file name (/run_OV*V)
    Temperature = Temperature at which data was taken. Taken from run_map

    SupVolt     = Over voltage according to run_map. SupVolt = Vbias - (Vindac + Vbd[from dict_OV_map]). 
                  Vbias = Vbias taken from run_map + multimeter_Vdif
                  therefore:
                     [SupVolt = Vbias(from run_map) + multimeter_Vdif - (Vindac + Vbd[from dict_OV_map])]
                  SupVolt needs to be corrected later on for the differences in temperature. 
                  (Not done in this script)

    ADC, ADC_err                 = fit parameters for MIP MPV.
    LandauWidth, LandauWidth_err = fit parameters for Landau width
    GausWidth, GausWidth_err     = fit parameters for Gaussian width
    TotalWidth, TotalWidth_err   = fit parameters for MIP width (Landau + Gaus)
    Ped_RMS, RMS_error           = fit parameters for MIP MPV

-------------------
NOTE: 
If the run contains only data from irradiated SiPMs (or irradiated SiPMs in general) raise "is_irrad" flag to 1.
And set the channel numbers for the irradiated SiPMs as well as the far channel (for more info read in-line comments)

"""

import pandas as pd
import numpy as np
from glob import glob
import os, sys

import matplotlib as mpl
import matplotlib.pyplot as plt

from langaus import LanGausFit
import langaus
import ROOT, uproot

import math 

# ROOT.EnableImplicitMT(8)

# set font size
font = {'size'   : 20}
mpl.rc('font', **font)


################### Variables ################################

# input folders
map_fname = "RunMaster.txt"     # Contains all information about each individual data run
                                # columns in map_fname = run_nr, Vbias, channel, ConvGain, Temp, Config

# Output File Name
outputHDF5file = "/df_TestBeam.h5"

is_irrad = 0                            # if irradiated channels are present, raise flag to 1
irrad_channels = [36,37,64,65]          # Irradiated channel list


# The channel furthest away from irradiated channel which data was also taken for. 
# This data run is used to get the pedestal information from the irradiated channel 
far_chan = {36:64,37:65,64:36,65:37,
            47:57,45:55,51:61,49:59,
            48:58,46:56,52:62,50:60,
            57:47,55:45,61:51,59:49,
            58:48,56:46,62:52,60:50}

multimeter_Vdif = {"TB3_1":0.03,"TB3_2":0.03,"mini":0}   # Difference between multimeter and Vout of TB-tester DAQ
Vindac_nonirr = 1.9                                      # voltage for inputdac=31. ( OV = Vbias - (Vindac + Vbd) )
Vindac_nonirr_err = 0.010

# if the inputdac is not 31 for irradiated channels
Vindac_irr = {36:1.055,64:1.361,37:1.625,65:1.713}
Vindac_irr_err = 0.005

####################### DATA CUTS #############################

ADCcut4TOA = "adc > 80"                 # ADC cut for time walk reduction 
TOAcut4TOA = "toa >  5 && toa < 1020"   # TOA cut for rescaling 


######################## Statics ##############################

# breakdown voltages
dict_OV_map    = {"TB3_1":38.00,"TB3_2":38.00,"mini":38.00}

# breakdown voltage errors
Vbd_nonirr_err = {"TB3_1":0.10,"TB3_2":0.10,"mini":0.10}
Vbd_irr_err = {"TB3_1":0.10,"TB3_2":0.10,"mini":0.10}

###############################################################

def main(base_dir,Tileboard):
    df_width = pd.DataFrame(columns=["channel","Config","Tileboard","ConvGain","OverVolt","SupVolt","SupVolt_error",
                                    "ADC","ADC_error","LandauWidth","LandauWidth_error","GausWidth","GausWidth_error",
                                    "TotalWidth","TotalWidth_error","Ped_RMS","RMS_error","Temperature"])

    
    run_map = pd.read_csv(base_dir + "/" + map_fname, sep = "\t")
    fnames = run_map["run_nr"]
    print("Found %i data files" % len(fnames))

    nfiles = 0
    df_chans = pd.DataFrame()
    
    if is_irrad == 1:
        """
        PEDESTAL CALCULATION
        
        Pedestal is calculated by taking a a data run belonging to the far_chan and plotting the histogram of the channel needed
        
        """
#         irrad_df = pd.DataFrame(columns=["channel","OV","ConvGain","SupVolt","Mean","Mean_err","StdDev","StdDev_err"])
        irrad_df = pd.DataFrame(columns=["channel","OV","ConvGain","Mean","Mean_err","StdDev","StdDev_err"])
        
        count = 0
        for run_nr in fnames:
            # Read run_map and read the needed run details 
            s = run_map[run_map.run_nr==run_nr]
            Vbias = s.Vbias.values[0]
            chan  = s.channel.values[0]
            ConvGain = s.ConvGain.values[0]
            Temp = s.Temp.values[0]
            Config = s.Config.values[0] 

            OV_pure = run_nr[run_nr.find("OV")+2:run_nr.find("_202")]
            OV = float(OV_pure.replace("V", "."))
            
            # calculate the SupVolt (not used due to redundancy!! The value from data run is used instead)
            SupVolt = np.round(Vbias + multimeter_Vdif[Tileboard] - (dict_OV_map[Tileboard] + Vindac_irr[chan]),2)

            # Open root file and convert to pandas DataFrame
            fname = base_dir + "/"+run_nr+"/beam_run0.root"
            file = uproot.open(fname)
            tree = file["unpacker_data"]["hgcroc"]
            # df = tree.arrays(library="pd")         # for LCG > 101
            df_chans = tree.pandas.df()              # for LCG =  99
        
            # Read channel number
            # CURRENTLY ONLY SECOND HALF OF TILEBOARD IS IN USE
            df_chans["chan_proper"] = df_chans["half"]*36 + df_chans["channel"]
            df_chans = df_chans[df_chans.half==1]
            
            df_chans_mean = df_chans.groupby(['chan_proper'])['adc'].mean().reset_index()
            df_chans_median = df_chans.groupby(['chan_proper'])['adc'].median().reset_index()
            df_chans_stddev = df_chans.groupby(['chan_proper'])['adc'].std().reset_index()
            
            # Convert pandas DataFrame to RDataFrame
            df_irrad = df_chans[df_chans.chan_proper==far_chan[chan]]
#             print(df_irrad.head())
            df_irrad_rdf = {key: df_irrad[key].values for key in df_irrad.columns}
            df_irrad_rdf = ROOT.RDF.MakeNumpyDataFrame(df_irrad_rdf)
            
            
            # Plot Pedestal histogram and fit Gaussian function
            ROOT.gStyle.SetOptStat(0)
            canvasPed = ROOT.TCanvas("canvasPed","canvasPed",800, 600)
            canvasPed.SetFillColor(0)
            canvasPed.SetGrid()
            canvasPed.GetFrame().SetFillColor(2)
            canvasPed.GetFrame().SetBorderSize(12)

            histPed = df_irrad_rdf.Histo1D(("dataPed", "dataPed", int(200), 0,400), "adc")
            histPed.Draw()
            histPed.GetXaxis().SetTitle("Pedestal Amplitude [ADC]")
            histPed.GetYaxis().SetTitle("No. of entries")
            
            func = ROOT.TF1("func","gaus",0,300);
            func.SetParameters(histPed.GetMaximum(), histPed.GetMean(), histPed.GetRMS() ); 
            histPed.Fit("func")
            
            l1=ROOT.TLegend(0.30,0.72,0.32,0.88)
            l1.SetHeader("For Ch:{} at ConvGain:{} and Overvoltage:{} V".format(
                far_chan[chan],ConvGain,np.round(SupVolt,2)),"l") # option "C" allows to center the header

    #         l1.AddEntry(func,"no. of events = {}".format(len(df_psel),"l"))
            l1.AddEntry(func,"Mean = ({}+/-{}) ADC".format(round(func.GetParameter(1),2),round(func.GetParError(1),2)),"l")
            l1.AddEntry(func,"Std. Dev = ({}+/-{}) ADC".format(round(func.GetParameter(2),2),round(func.GetParError(2),2)),"l")
            l1.SetMargin(0.1);
            l1.SetMargin(0.1);
            l1.SetTextSize(0.04);
            l1.SetTextFont(42);
            l1.SetBorderSize(0);
            l1.Draw()

            # Save in individual run folders
            plot_dir=base_dir+run_nr+"/PedestalPlot/"
            if not os.path.exists(plot_dir):os.makedirs(plot_dir)
            canvasPed.SaveAs(plot_dir+'/Ch{}_ConvGain{}.png'.format(far_chan[chan],ConvGain))
            canvasPed.SaveAs(plot_dir+'/Ch{}_ConvGain{}.C'.format(far_chan[chan],ConvGain))
            print("saved in "+plot_dir) 
            
            # save in Pedestal_config_{}
            plot_dir=base_dir+"/Pedestal_Config"+str(Config)+"/OverVolt_"+ OV
            if not os.path.exists(plot_dir):os.makedirs(plot_dir)
            canvasPed.SaveAs(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,ConvGain))
            canvasPed.SaveAs(plot_dir+'/Ch{}_ConvGain{}.C'.format(chan,ConvGain))
            print("saved in "+plot_dir) 

            canvasPed.Draw()
            canvasPed.Close()
            func.GetParameter(0)

            # Save information in irrad_df DataFrame
            irrad_df = irrad_df.append(pd.DataFrame([[far_chan[chan],OV,ConvGain,
                                                      func.GetParameter(1),func.GetParError(1),
                                                      func.GetParameter(2),func.GetParError(2)]]
                                                    ,columns=irrad_df.columns))
            
            count += 1
            if count%5==0 : print(str(count),"files completed")
            
        print(irrad_df)
        
        
    for run_nr in fnames:
        """
        DATA RUN
            
        Use TrigTime to fit the pulse and get the histogram to calculate MIP value 
        """
        print(run_nr)

        # Read run_map and read the needed run details 
        s = run_map[run_map.run_nr==run_nr]
        Vbias = s.Vbias.values[0]
        chan  = s.channel.values[0]
        ConvGain = s.ConvGain.values[0]
        Temp = s.Temp.values[0]
        Config = s.Config.values[0]
        trigmin = s.TrigMin.values[0]   
        trigmax = s.TrigMax.values[0]
        PedThresh = s.PedThresh.values[0]
        print("trig min:",trigmin,"trig max:",trigmax)

        OV = s.OV.values[0]
        try:
            SupVolt = np.round(Vbias + multimeter_Vdif[Tileboard] - (dict_OV_map[Tileboard] + Vindac_irr[chan]),2)
            SupVolt_err = (Vindac_irr_err**2 + Vbd_irr_err[Tileboard]**2)**0.5
        except KeyError:
            print(Vbias)
            print(multimeter_Vdif[Tileboard])
            print(dict_OV_map[Tileboard])
            print(Vindac_nonirr)
            SupVolt = np.round(Vbias + multimeter_Vdif[Tileboard] - (dict_OV_map[Tileboard] + Vindac_nonirr),2)
            SupVolt_err = (Vindac_nonirr_err**2 + Vbd_nonirr_err[Tileboard]**2)**0.5
            
        print("Vbias:",Vbias," chan",chan," ConvGain:",ConvGain)

        # Open root file and convert to pandas DataFrame
        fname = base_dir + "/"+run_nr+"/beam_run0.root"
        file = uproot.open(fname)
        tree = file["unpacker_data"]["hgcroc"]
        # df = tree.arrays(library="pd")               # for LCG > 101
        df_chans = tree.pandas.df()                    # for LCG =  99
    
        # Read channel number
        if chan > 36:
            #df_chans["chan_proper"] = df_chans["half"]*36 + df_chans["channel"]
            sel  = df_chans.channel==int(chan)-36 
            sel &= df_chans.half==1
            df_chans = df_chans[sel]
        else:
            sel  = df_chans.channel==int(chan)
            sel &= df_chans.half==0
            df_chans = df_chans[sel]

#         print("hit chans:{}".format(hit_chans))
        print("selected chan:{}".format(chan))

        df_chans["ConvGain"] = ConvGain
        df_chans["Temp"] = Temp
        df_chans["SupVolt"] = SupVolt
        
        print(df_chans.head())    
        #df_psel = df_chans[df_chans.chan_proper==chan]
        df_psel = df_chans[df_chans.corruption==0]
        print("df_psel length before cut:",len(df_psel))

        # Convert pandas DataFrame to RDataFrame
        df_psel_rdf = {key: df_psel[key].values for key in df_psel.columns}
        df_psel_rdf = ROOT.RDF.MakeNumpyDataFrame(df_psel_rdf)

        # apply data cuts
        
        df_psel_rdf2 = df_psel_rdf.Filter(ADCcut4TOA)
        df_psel_rdf2 = df_psel_rdf2.Filter(TOAcut4TOA)
        
        trigmin_data = df_psel_rdf.Min("trigtime").GetValue()
        trigmax_data = df_psel_rdf.Max("trigtime").GetValue()

        TOAmin = df_psel_rdf.Min("toa").GetValue()
        if TOAmin < 0 or TOAmin > 1200: TOAmin = 0
        TOAmax = df_psel_rdf.Max("toa").GetValue()
        if TOAmax < 0 or TOAmax > 1200: TOAmax = 1024
            
        TOAdif = TOAmax-TOAmin
        print("TOAmin:",TOAmin)
        print("TOAmax:",TOAmax)
        print("TOAdif:",TOAdif)
#             ADCmin = df_psel_rdf2.Min("adc").GetValue()
        ADCmin = PedThresh - 100
        ADCmax = df_psel_rdf.Max("adc").GetValue()

    
        # Plot TOA vs TrigTime and Save plot
        canvas0 = ROOT.TCanvas("canvas0","canvas0",800, 600)
        canvas0.SetFillColor(0)
        canvas0.SetGrid()
        canvas0.GetFrame().SetFillColor(2)
        canvas0.GetFrame().SetBorderSize(12)

        histPulse = df_psel_rdf2.Histo2D(("histPulse", "", int(trigmax_data-trigmin_data), trigmin_data, trigmax_data,int((TOAmax-TOAmin)/5), TOAmin, TOAmax), "trigtime",  "toa")
        histPulse.Draw("colz")
        histPulse.GetXaxis().SetTitle("TrigTime [ADC]")
        histPulse.GetYaxis().SetTitle("TOA [ADC]")

        plot_dir=base_dir+run_nr+"/TOAvsTrigTime/"
        if not os.path.exists(plot_dir):os.makedirs(plot_dir)
        canvas0.SaveAs(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,ConvGain))
        canvas0.SaveAs(plot_dir+'/Ch{}_ConvGain{}.C'.format(chan,ConvGain))
        print("saved in "+plot_dir) 
        canvas0.Draw()

        # Plot ADC vs TrigTime and Save plot
        canvas01 = ROOT.TCanvas("canvas01","canvas01",800, 600)
        canvas01.SetFillColor(0)
        canvas01.SetGrid()
        canvas01.GetFrame().SetFillColor(2)
        canvas01.GetFrame().SetBorderSize(12)

        # Plot limits 
        # for 4mm² SiPMs
        if   ConvGain <= 4 :   uplim = 500
        elif ConvGain <= 12:   uplim = 500
        elif ConvGain <= 15:   uplim = 600
            
        # for 9mm² SiPMs
        if   ConvGain <= 4 :   uplim = 700
        elif ConvGain <= 12:   uplim = 900
        elif ConvGain <= 15:   uplim = 1000

        if OV <= 1.6    :  uplim -= 200
        elif OV <= 1.8  :  uplim -= 100
        elif OV >= 3.8  :  uplim += uplim
        elif OV >= 5.8  :  uplim += uplim*3

        if uplim >1020: uplim=1020
        
        histPulse = df_psel_rdf.Histo2D(("histPulse", "", int(trigmax_data-trigmin_data), trigmin_data, trigmax_data, int((ADCmax-ADCmin)/20), ADCmin, ADCmax), "trigtime","adc")
        histPulse.Draw("colz")
        histPulse.GetXaxis().SetTitle("TrigTime [ADC]")
        histPulse.GetYaxis().SetTitle("Amplitude [ADC]")

        plot_dir=base_dir+run_nr+"/ADCvTrigTime/"
        if not os.path.exists(plot_dir):os.makedirs(plot_dir)
        canvas01.SaveAs(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,ConvGain))
        canvas01.SaveAs(plot_dir+'/Ch{}_ConvGain{}.C'.format(chan,ConvGain))
        print("saved in "+plot_dir) 
        canvas01.Draw()
        
        # Data cuts to get the histogram at the peak of the pulse
        df_psel = df_psel[df_psel.trigtime>trigmin]
        df_psel = df_psel[df_psel.trigtime<trigmax]
        df_psel = df_psel[df_psel.corruption==0]
        df_psel_rdf = {key: df_psel[key].values for key in df_psel.columns}
        df_psel_rdf = ROOT.RDF.MakeNumpyDataFrame(df_psel_rdf)
        print("df_psel length after cut:",len(df_psel))

#         plot_dir=base_dir+"/root_files_nl1a/"
#         if not os.path.exists(plot_dir):os.makedirs(plot_dir)
#         df_psel_rdf.Snapshot("data",plot_dir+run_nr+".root")
#         print("Saved as " +  plot_dir+run_nr+".root")

        # Plot the histogram of ADC and fit Landau-Gaus convoluted function
        nbins = int(1020/4)
        canvas1 = ROOT.TCanvas("canvas1","canvas1",800, 600)
        canvas1.SetFillColor(0)
        canvas1.SetGrid()
        canvas1.GetFrame().SetFillColor(2)
        canvas1.GetFrame().SetBorderSize(12)

        histLangaus = df_psel_rdf.Histo1D(("dataLangaus", "dataLangaus", nbins, 0,1020), "adc")
        histLangaus.Draw()    

        if is_irrad ==0:
            """
            If unirradiated SiPMs : GET PEDESTAL PEAK FROM SAME DATA RUN
            
            NOTE: For this to work, the data threshold (lolim) separating pedestal and data must be 
            large enough to have the complete pedestal peak separated and 
            the pedestal peak must have a higher hit count than data peak that may be there
            """
            print("lolim:",PedThresh,"uplim:",uplim)
            histLangaus.SetAxisRange(PedThresh,uplim,"X")
            yuplim_val = histLangaus.GetXaxis().GetBinCenter(histLangaus.GetMaximumBin());
#             print("yuplim_val:",yuplim_val)
            if yuplim_val<PedThresh: yuplim_val = PedThresh * 1.5
#             print("yuplim_val:",yuplim_val)
            yuplim = histLangaus.GetBinContent(histLangaus.GetMaximumBin());

            histLangaus.SetAxisRange(0,yuplim,"Y")

        histLangaus.SetAxisRange(ADCmin,uplim,"X")

        
        histLangaus.GetXaxis().SetTitle("Reconstructed Amplitudes [ADC]")
        histLangaus.GetYaxis().SetTitle("No. of entries")
        histLangaus.GetXaxis().SetLabelSize(0.05);
        histLangaus.GetXaxis().SetTitleSize(0.05);
        histLangaus.GetXaxis().SetNdivisions(505);
        histLangaus.GetXaxis().SetTitleOffset(1.0);
        histLangaus.GetYaxis().SetTitleOffset(1.0);
        histLangaus.GetYaxis().SetLabelSize(0.05);
        histLangaus.GetYaxis().SetTitleSize(0.05);

        
        if is_irrad == 1:
            cut  = irrad_df.channel == chan
            cut &= irrad_df.OV == OV
            cut &= irrad_df.ConvGain == ConvGain
            pedestal = irrad_df[cut].Mean.values[0]
            ped_err = irrad_df[cut].Mean_err.values[0]
            ped_std = irrad_df[cut].StdDev.values[0]
            ped_std_err = irrad_df[cut].StdDev_err.values[0]
            
        else:
            pedestal = df_psel[df_psel.adc<PedThresh].adc.median()
            ped_err = pedestal*0.01
            ped_std = 2.0
            ped_std_err = ped_std*0.01
        
        
        fitrange_low  = (yuplim_val)*0.7
        fitrange_high = (yuplim_val)*1.3
        if fitrange_high >1000: fitrange_high = 1000
        
        fit = LanGausFit()
#             func = fit.fit(histLangaus,fitrange=(lolim,uplim-50))
        func = fit.fit(histLangaus,fitrange=(fitrange_low,fitrange_high),startmpv=yuplim_val)
#             func = fit.fit(histLangaus,fitrange=(yuplim_val*0.7,uplim-50))


        print("pedestal:",pedestal,"+/-",ped_err)
        print("std. dev:",ped_std,"+/-",ped_std_err)

        wdth        = func.GetParameter(0)
        wdth_err    = func.GetParError(0)
        mpv         = func.GetParameter(1)
        mpv_err     = func.GetParError(1)
        norm        = func.GetParameter(2)
        norm_err    = func.GetParError(2)
        mu          = func.GetParameter(3)
        mu_err      = func.GetParError(3)
        mpv_lan     = func.GetMaximumX()
        mpv_lan_err = (mpv_err/mpv)*(mpv_lan)

        mpv_WOped  = mpv_lan - pedestal
        mpv_WOped_err  = (mpv_lan_err**2 + ped_err**2)**0.5
        snr = mpv_WOped/ped_std
        
        error = ((wdth)**2 + (mu)**2)**(1/2)
        error_err = np.sqrt((wdth**2 * wdth_err**2 + mu**2 * mu_err**2)/(wdth**2 + mu**2))
        chisq = func.GetChisquare()/func.GetNDF()

    #     In this way you find the maximum
        y_max = func.GetMaximum();
        x_max = func.GetMaximumX();

    #     //let's search for the point of half maximum, 
    #     //one will be on the left and the other one on the right
    #     //so the reason for the two call, using he lower and upper bound
    #     //of the x range for the fuction.

        fwhm_left = func.GetX(y_max/2, x_max);
        fwhm_right = func.GetX(y_max/2, x_max, df_psel.adc.mean()+3*df_psel.adc.std());
        fwhm = fwhm_right - fwhm_left
        print("FWHM/2.3 = " + str(fwhm/2.3))

        fwhm_left = func.GetX(9*y_max/10, x_max);
        fwhm_right = func.GetX(9*y_max/10, x_max, df_psel.adc.mean()+3*df_psel.adc.std());
        fwhm = fwhm_right - fwhm_left
        print("FW90%M = " + str(fwhm))
        func.Draw("same")

        ROOT.gStyle.SetOptTitle(0)
        ROOT.gStyle.SetOptStat(0)
        l1=ROOT.TLegend(0.2,0.60,0.88,0.88)
        l1.SetFillStyle(0)
        
        l1.SetHeader("For Ch:{} at ConvGain:{} and Overvoltage:{}+/-{}".format(chan,ConvGain,
                                                                               np.round(SupVolt,2),
                                                                               np.round(SupVolt_err,2)),"") # option "C" allows to center the header
        l1.SetTextAlign(33)
        l1.AddEntry(0,"fit maximum = ({}+/-{}) ADC".format(round(mpv_lan,2),round(mpv_lan_err,2)),"")
        l1.AddEntry(0,"pedestal = ({}+/-{})".format(round(pedestal,2),round(ped_err,2)),"")
        l1.AddEntry(0,"fit max. (ped subed) = ({}+/-{}) ADC".format(round(mpv_WOped,2),round(mpv_WOped_err,2)),"")
        l1.AddEntry(0,"fit width = ({}+/-{}) ADC".format(round(error,2),round(error_err,2)),"")
        
        if is_irrad == 0 : 
            l1.AddEntry(func,"Chi/NDF = {}; no. of events = {}".format(round(chisq,2),len(df_psel)),"")
        if is_irrad == 1 : 
            l1.AddEntry(func,"Chi/NDF = {}; no. of events = {}; S/R = {}".format(round(chisq,2),len(df_psel),round(snr,1)),"")
#             l1.AddEntry(func,"RMS = {}".format(round(df_psel.ADC.std(),2)),"l")
        l1.SetMargin(0.1);
        l1.SetTextSize(0.04);
        l1.SetTextFont(42);
        l1.SetBorderSize(0);
        l1.Draw()

        print("fit MIP for Ch:{} ={}+/-{}, gain:{}".format(chan,round(mpv_WOped,2),round(mpv_WOped_err,2),ConvGain))
        print("fit LandauWidth for Ch:{} ={}+/-{}, gain:{}".format(chan,round(wdth,2),round(wdth_err,2),ConvGain))
        print("fit sigma for Ch:{} ={}+/-{}, gain:{}".format(chan,round(mu,2),round(mu_err,2),ConvGain))
        print(len(df_psel))

        df_width = df_width.append(pd.DataFrame([[int(chan),Config,Tileboard,ConvGain,OV,SupVolt,SupVolt_err, 
                                                mpv_WOped,mpv_WOped_err,wdth,wdth_err,mu,mu_err,
                                                error,error_err,ped_std,ped_std_err,Temp]],
                                                columns=df_width.columns))

        # Save plots
        plot_dir=base_dir+run_nr+"/Landau_dist"
        if not os.path.exists(plot_dir):os.makedirs(plot_dir)
        canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,ConvGain))
        canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}.C'.format(chan,ConvGain))
        print("saved in "+plot_dir) 

        plot_dir=base_dir+"/Landau_dist_"+str(Config)+"/OverVolt_"+ str(OV) + "V"
        if not os.path.exists(plot_dir):os.makedirs(plot_dir)
        canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,ConvGain))
        canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}.C'.format(chan,ConvGain))
        print("saved in "+plot_dir) 

        canvas1.Draw()
        
        canvas0.Close()
        canvas01.Close()
        canvas1.Close()


    # Save dataframe
    print("Saving dataframe as " + base_dir + outputHDF5file)
    df_width.to_hdf(base_dir + outputHDF5file, key = "data")

        

if __name__ == "__main__":
    if len(sys.argv) == 3:
        base_dir = sys.argv[1]
        Tileboard = sys.argv[2]
        main(base_dir,Tileboard)

    else:
        print("No argument given ("+str(len(sys.argv))+")")
        print(sys.argv)
