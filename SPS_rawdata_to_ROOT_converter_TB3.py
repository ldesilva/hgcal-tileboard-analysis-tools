"""
LED DATA CONVERSION SCRIPT

Converts individual ROOT files into a single ROOT file for further processing.

Only can be used for data taken with TB-tester DAQ system
CANNOT BE USED WITH KCU105 DAQ DATA

----------------
TO EXECUTE

python3 SPS_rawdata_to_ROOT_converter_TB3.py <path/to/ROOT files>

----------------
RETURNS

root_SPS_***_ref0.root, root_SPS_***_refSeparated.root and root_SPS_***_refcorrected.root 


root_SPS_***_refcorrected.root 

    Contains a root file with the following columns (key="data")

                ["channel","ConvGain","BoardConfig","OverVolt","extLED",
                 "offset","phase","trim","bxID","adc","tot","toa"]


    channel     = HGCROC channel number according to the tileboard floorplan. 
                  Not to confuse with the raw HGCROC channels as that cordinate system is different
    ConvGain    = Conveyor Gain. Default is 12. Value taken from run_map. 
    OverVolt    = Over voltage according to file name (/run_OV*V)
    
    extLED      = LED bias voltage supplied
    offset      = L1A offset set (in order to sample pulse)
    phase       = phase offset setting (16 phases = 1 bunch crossing = 25 ns delay) 

    adc         = ADC measured by HGCROC
    toa         = time of arrival in ADC
    tot         = time over threshold in ADC
    
    trim      = trim parameter set. 
                  trim = 0 : Channel's nominal/default value  
                  trim = 1 : Channel's nominal/default value + 1
                  trim = 2 : Channel's nominal/default value + 2

                The different trim values are used to reduce the effect of the differential non-linearity (DNL) 
                present in the HGCROC. 
                Each increment of trim changes the pedestal by roughly 3 ADC bins. 
                The DNL is worst at every 2nd and 4th bin.
                Therefore by taking three data sets, subtracting the pedestal offset of 3 and 6 ADCs for trim=1 and trim=2
                respectively and then adding the three datasets together, one can minimize the DNL effect on the ADC. 
                
                NOTE: work is on-going to use a floating average method using just one of the datsets. 

note: if you want to use the data from a limited number of channels indicate in 'chan_list'

"""

import pandas as pd
import numpy as np
from glob import glob
import os,sys

import ROOT

import matplotlib as mpl
import matplotlib.pyplot as plt

import seaborn as sns

# set font size
font = {'size'   : 20}
mpl.rc('font', **font)
# mpl.rcParams['text.usetex'] = True

import uproot

chan_list = [13,14,15,51]
# chan_list = range(0,72)

def main(base_dir):
    # read dataframes
    
    fnames = glob(base_dir + "/TB*/sampling_scan*.root")
    print("Found %i files" % len(fnames))
    
    name_dir = base_dir.split("/")[-1]
    if name_dir=="": name_dir = base_dir.split("/")[-2]
    print("name_dir:",name_dir)
    
    dfs = pd.DataFrame()

    for fname in sorted(fnames):
        try:
            print(fname)
            file = uproot.open(fname)
            tree = file["unpacker_data"]["hgcroc"]
            #     df = tree.arrays(library="pd")     # for LCG > 101
            df = tree.pandas.df()                    # for LCG =  99

            # Add the information on overvoltage, configuration, LED bias voltage, trim etc to the dataframe
            
            df['chan'] = df['channel'] + df['half']*36
            sel = df.chan==chan_list[0]
            for chan in chan_list:
                sel |=  df.chan==chan
            df = df[sel]
            
            if fname.find("_OV") >0 : 
                start = int(fname.find("_OV") + 3)
                OV = int(fname[start:start+1])
                df["OverVolt"] = OV
            else: 
                print("no OverVolt. Returning 4V (default)")
                OV = 4

#             if fname.find("miniTB/") >0 : 
#                 df["Tileboard"] = "miniTB"
#             elif fname.find("Board1/") >0 : 
#                 df["Tileboard"] = "TB3_1"
#             elif fname.find("Board2/") >0 : 
#                 df["Tileboard"] = "TB3_2"
#             else : 
#                 df["Tileboard"] = "TB3"

            if fname.find("Config") >0 : 
                df["BoardConfig"] = int(fname[fname.find("Config")+6:fname.find("Config")+7])
            elif fname.find("config") >0 : 
                df["BoardConfig"] = int(fname[fname.find("config")+6:fname.find("config")+7])
            else : df["BoardConfig"] = 1
    #             df = df.astype({'BoardConfig': 'int32'}).dtypes
    #             print(df["BoardConfig"])

            if fname.find("Phsstart") >0 : 
                ph = fname[fname.find("Phsstart")+8:fname.find("Phsstart")+10]
                if (ph[1]=="_" or ph[1]=="/"): ph = ph[0]
                ph = int(ph)
    #             print('ph:',ph)

            if fname.find("Phsstop") >0 : 
                phs = fname[fname.find("Phsstop")+7:fname.find("Phsstop")+9]
                if (phs[1]=="_" or phs[1]=="/"): phs = phs[0]
                phs = int(phs)
    #             print('phs:',phs)

            if fname.find("Phsstep") >0 : 
                phstp = fname[fname.find("Phsstep")+7:fname.find("Phsstep")+9]
                if (phstp[1]=="_" or phstp[1]=="/"): phstp = phstp[0]
                phstp = int(phstp)
    #             print('phstp:',phstp)


            if fname.find("BXstart") >0 : 
                of = fname[fname.find("BXstart")+7:fname.find("BXstart")+9]
                if (of[1]=="_" or of[1]=="/"): of = of[0]
                of = int(of)
    #             print('of:',of)


            if fname.find("BXstop") >0 : 
                ofs = fname[fname.find("BXstop")+6:fname.find("BXstop")+8]
                if (ofs[1]=="_" or ofs[1]=="/"): ofs = ofs[0]
                ofs = int(ofs)
    #             print('ofs:',ofs)


            if fname.find("BXstep") >0 : 
                ofstp = fname[fname.find("BXstep")+6:fname.find("BXstep")+8]
                if (ofstp[1]=="_" or ofstp[1]=="/"): ofstp = ofstp[0]
                ofstp = int(ofstp)
    #             print('ofstp:',ofstp)


            if fname.find("trimstart") >0 : 
                trm = fname[fname.find("trimstart")+9:fname.find("trimstart")+10]
                trm = int(trm)
    #             print('trm:',trm)

            if fname.find("trimstop") >0 : 
                trms = fname[fname.find("trimstop")+8:fname.find("trimstop")+9]
                trms = int(trms)
    #             print('trms:',trms)

            if fname.find("trimstep") >0 : 
                trmstp = fname[fname.find("trimstep")+8:fname.find("trimstep")+9]
                trmstp = int(trmstp)
    #             print('trmstp:',trmstp)


            if fname.find("sampling_scan") >0 : 
                scan_nr = fname[fname.find("sampling_scan")+13:fname.find("sampling_scan")+16]
    #             print('scan_nr:',scan_nr)
                if (scan_nr[2]=="."): scan_nr = scan_nr[:2]
    #             print('scan_nr:',scan_nr)
                if (scan_nr[1]=="."): scan_nr = scan_nr[0]
                scan_nr = int(scan_nr)
    #             print('scan_nr:',scan_nr)


            df['phase'] = int(scan_nr%((phs-ph+1)/phstp)) 
            offsettrim  = int(scan_nr/((phs-ph+1)/phstp)) 
            nr_trims    = int(offsettrim/((ofs-of+1)/ofstp)) 
            nr_offsets  = int(nr_trims%((ofs-of+1)/ofstp)) 
            print(scan_nr%((phs-ph+1)/phstp),nr_trims,nr_offsets+of)
            df['offset'] = nr_offsets+of
            df['trim']   = nr_trims  

            if fname.find("mV") >0 : 
                df["LED"] = int(fname[int(fname.find("mV"))-4:int(fname.find("mV"))])
            else: 
                print("no LED parameter. LED set to 6000 mV")
                df["LED"] = 6000

    #             if fname.find("_Temp") >0 : 
    #                 start = fname.find("_Temp") + 5
    #                 end = fname.find("p0/")
    #                 df["temperature"] = float(fname[start:end])
    #             else: df["temperature"] = 22.7

            if fname.find("ConvGain") >0 : 
                cg = fname[fname.find("ConvGain")+8:fname.find("ConvGain")+10]
                if cg[1] == "_": cg = cg[0]
                df["ConvGain"] = int(cg)
            else: 
                print("no ConvGain parameter. ConvGain set to 12")
                df["ConvGain"] = 12

            dfs=dfs.append(df)
            
        except KeyError:
            pass

    
    
    dfs_rdf = {key: dfs[key].values for key in dfs.columns}
    
    # save data set as ***_refSeparated.root
    rdfs = ROOT.RDF.MakeNumpyDataFrame(dfs_rdf)
    rdfs.Snapshot("data", base_dir+"/root_"+name_dir+"_trimSeparated.root")
    print("Saved as " + base_dir+"/root_"+name_dir+"_trimSeparated.root")

    # dfs.to_hdf(base_dir+"/root_"+name_dir+"_ref0.h5", key = "data")
    
    # save trim=0 data as ***_ref0.root
    dfs0 = dfs[dfs.trim == 0]
    dfs0_rdf = {key: dfs0[key].values for key in dfs0.columns}

    rdfs0 = ROOT.RDF.MakeNumpyDataFrame(dfs0_rdf)
    rdfs0.Snapshot("data", base_dir+"/root_"+name_dir+"_trim0.root")
    print("Saved as " + base_dir+"/root_"+name_dir+"_trim0.root")

    df_chans = dfs
    print(dfs)
    
    # do the trim correction : trim=1 => adc=adc-3     and     trim=2 => adc=adc-6
    df_chans['adc'] = np.where(df_chans['trim'] == 2, df_chans['adc']-2, df_chans['adc'])
    df_chans['adc'] = np.where(df_chans['trim'] == 1, df_chans['adc']-1, df_chans['adc'])

    # print(dfs)
    print("ref merge complete")

    df_chans_rdf = {key: df_chans[key].values for key in df_chans.columns}
    
    # save trim corrected dataset as ***_refcorrected.root
    rdf_chans = ROOT.RDF.MakeNumpyDataFrame(df_chans_rdf)
    rdf_chans.Snapshot("data", base_dir+"/root_"+name_dir+"_OV"+str(OV)+"_refcorrected.root")
    print("Saved as " + base_dir+"/root_"+name_dir+"_OV"+str(OV)+"_refcorrected.root")

    # df_chans.to_hdf(base_dir+"/root_"+name_dir+"_refcorrected.h5", key = "data")



if __name__ == "__main__":
    if len(sys.argv) == 2:
        fname = sys.argv[1]
        print(fname)
        main(fname)

    else:
        print("No argument given")
        print(len(sys.argv))
    