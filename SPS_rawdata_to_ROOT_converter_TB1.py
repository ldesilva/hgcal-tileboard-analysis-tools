"""
LED DATA CONVERSION SCRIPT

Converts individual dataframes created after running process_SPS.py into a single ROOT file for further processing.

Only can be used for data taken with KCU105 DAQ system
CANNOT BE USED WITH TB-TESTER DAQ DATA

----------------
TO EXECUTE

python3 SPS_rawdata_to_ROOT_converter_TB1.py <path/to/dataframes>

----------------
RETURNS

root_SPS_***_ref0.root and root_SPS_***_refcorrected.root 


root_SPS_***_refcorrected.root 

    Contains a root file with the following columns (key="data")

                ["channel","ConvGain","BoardConfig","OverVolt","extLED",
                 "offset","phase","refinv","bxID","adc","tot","toa"]


    channel     = HGCROC channel number according to the tileboard floorplan. 
                  Not to confuse with the raw HGCROC channels as that cordinate system is different
    ConvGain    = Conveyor Gain. Default is 12. Value taken from run_map. 
    OverVolt    = Over voltage according to file name (/run_OV*V)
    
    extLED      = LED bias voltage supplied
    offset      = L1A offset set (in order to sample pulse)
    phase       = phase offset setting (16 phases = 1 bunch crossing = 25 ns delay) 

    adc         = ADC measured by HGCROC
    toa         = time of arrival in ADC
    tot         = time over threshold in ADC
    
    refinv      = ref_inv_dac parameter set. 
                  refinv = 0 : Channel's nominal/default value  
                  refinv = 1 : Channel's nominal/default value + 1
                  refinv = 2 : Channel's nominal/default value + 2

                The different refinv values are used to reduce the effect of the differential non-linearity (DNL) 
                present in the HGCROC. 
                Each increment of refinv changes the pedestal by roughly 3 ADC bins. 
                The DNL is worst at every 2nd and 4th bin.
                Therefore by taking three data sets, subtracting the pedestal offset of 3 and 6 ADCs for refinv=1 and refinv=2
                respectively and then adding the three datasets together, one can minimize the DNL effect on the ADC. 
                
                NOTE: work is on-going to use a floating average method using just one of the datsets. 

"""

import pandas as pd
import numpy as np
from glob import glob
import os, sys

import ROOT

import matplotlib as mpl
import matplotlib.pyplot as plt

# set font size
font = {'size'   : 20}
mpl.rc('font', **font)


def main(indir):
    # read dataframes
    fnames = glob(indir + "/dataframe_*.h5")
    #fnames = glob(base_dir + "/dataframe.h5")
    print("Found %i files" % len(fnames))

    dfs = pd.DataFrame()

    for fname in sorted(fnames):
        print(fname)
        df = pd.read_hdf(fname, key = "data")
        
        # Add the information on overvoltage, configuration, LED bias voltage, refinv etc to the dataframe
        
        if fname.find("_OV") >0 : 
            start = int(fname.find("_OV") + 3)
            df["OverVolt"] = int(fname[start:start+1])
        else: print("didnt work OverVolt")

        if fname.find("Config") >0 : 
            df["BoardConfig"] = int(fname[fname.find("Config")+6:fname.find("Config")+7])
        elif fname.find("config") >0 : 
            df["BoardConfig"] = int(fname[fname.find("config")+6:fname.find("config")+7])
        else : df["BoardConfig"] = 1

        if fname.find("extLED_") >0 : 
            start = int(fname.find("extLED_") + 7)
            end = int(fname.find("mV_"))
    #         print("base_dir")
    #         print(fname[start:end])
            df["LED"] = int(fname[start:end])
        else: print("didnt work")

        if fname.find("ConvGain_") >0 : 
            start = int(fname.find("ConvGain_") + 9)
            end = int(fname.find("_ext"))
    #         print("base_dir")
    #         print(fname[start:end])
            df["ConvGain"] = int(fname[start:end])
        else: print("didnt work Gain")
            
        if fname.find("offset_") >0 : 
            of = fname[fname.find("offset_")+7:fname.find("offset_")+8]
            df["offset"] = int(of)

        if fname.find("phase_") >0 : 
            ph = fname[fname.find("phase_")+6:fname.find("phase_")+8]
            if (ph[1]=="_" or ph[1]=="/"): ph = ph[0]
            df["phase"] = int(ph)

        if fname.find("refinv_") >0 : 
            of = fname[fname.find("refinv_")+7:fname.find("refinv_")+8]
            df["refinv"] = int(of)

        elif fname.find("ref") >0 : 
            of = fname[fname.find("ref")+3:fname.find("ref")+4]
            df["refinv"] = int(of)

        dfs=dfs.append(df)
    print(dfs.head())
    print(dfs.dtypes)
        
    base_dir = fname[:fname.find("/dataframe")]
    name_dir = fname[fname.find("/SPS_scan")+1:fname.find("/dataframe")]
    
    # save refinv=0 data as ***_ref0.root
    dfs0 = dfs[dfs.refinv == 0]
    dfs0_rdf = {key: dfs0[key].values for key in dfs0.columns}
    
    rdfs0 = ROOT.RDF.MakeNumpyDataFrame(dfs0_rdf)
    rdfs0.Snapshot("data", base_dir+"/root_"+name_dir+"_ref0.root")
    print("Saved as " + base_dir+"/root_"+name_dir+"_ref0.root")

    df_chans = dfs
    print(dfs)

    # do the refinv correction : refinv=1 => adc=adc-3     and     refinv=2 => adc=adc-6
    df_chans['adc'] = np.where(df_chans['refinv'] == 2, df_chans['adc']-6, df_chans['adc'])
    df_chans['adc'] = np.where(df_chans['refinv'] == 1, df_chans['adc']-3, df_chans['adc'])

    # print(dfs)
    print("ref merge complete")

    # save refinv corrected dataset as ***_refcorrected.root
    df_chans_rdf = {key: df_chans[key].values for key in df_chans.columns}

    rdf_chans = ROOT.RDF.MakeNumpyDataFrame(df_chans_rdf)
    rdf_chans.Snapshot("data", base_dir+"/root_"+name_dir+"_refcorrected.root")
    print("Saved as " + base_dir+"/root_"+name_dir+"_refcorrected.root")

    
if __name__ == "__main__":

    if len(sys.argv) == 2:
        indir = sys.argv[1]
        
        main(indir)

    else:
        print("No argument given")