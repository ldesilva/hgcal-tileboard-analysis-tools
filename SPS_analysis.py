"""
SPS GAIN CALCULATION USING LED DATA. 

THIS IS A SKELETON TO HELP KICK-START YOUR DATA ANALYSIS 

Each testbeam will have a file doing the SPS calculation as shown below. 

THIS IS ONLY A GUIDE! NOT AN ACTUAL SCRIPT DESIGNED TO WORK OUT OF THE BOX

"""

import pandas as pd
import numpy as np
from glob import glob
import os

import matplotlib as mpl
import matplotlib.pyplot as plt
%matplotlib inline

import seaborn as sns
import ROOT

# set font size
font = {'size'   : 20}
mpl.rc('font', **font)

import uproot

base_dir = "/nfs/dust/ilc/user/desilvam/DESYTB_Apr22_TB2_LED/"
# root_file = "/root_SPS_TB1p3.h5"

df_points = pd.DataFrame()

fnames = glob(base_dir + "/sps_gain_chansel*V.root")
# fnames = glob(base_dir + "/sps_gain_chansel*ref0.root")
print("Found %i files (TB2) in %s" % (len(fnames),base_dir))

for fname in fnames:
#     try:
    tree = uproot.open(fname)["T"]
    df = tree.pandas.df()
    df['Tileboard'] = 'TB2'
    try:
        print(df.ConvGain.unique())
    except AttributeError:
        df['ConvGain'] = 12
        
    df['BeamDate'] = 'Apr22'
    
    df_points = df_points.append(df)

# If TB2
df_points["channel"] = df_points["channel"] - 36
#####


# DATA CUTS
s0  = df_points['chi2ndf']    <   10

s = s0
df_points_chi = df_points[s]


# DATA OMISSIONS
s3  = df_points_chi['OverVolt']  ==  6
s3  = df_points_chi['channel']  ==    45

index = df_points_chi.index

indices = index[s]
indices_list = indices.tolist()

print(indices_list)  
df_points_chi = df_points_chi.drop(indices_list)

print(len(df_points_chi))

df_points_chi = df_points_chi.groupby(["channel","phase","offset","OverVolt","Tileboard","ConvGain","BeamDate"])['gain','gain_err'].mean().reset_index()


map_fname = "tmp_map_TB2.txt"
# map_fname = "tmp_map_invrt_tile.txt"

df_map = pd.read_csv(map_fname, sep = "\t")
#df_map.chan.hist(bins = range(80))
chs = df_map.chan.values
xs = df_map.x.values
ys = df_map.y.values

ch_map_x = dict(zip(chs,xs))
ch_map_y = dict(zip(chs,ys))

dict_OV_map_TB1p22 = {23:0.00001,25:0.00001,4:38.23,16:37.80,50:37.93,47:38.09,48:37.98,49:37.94,38:38.38,39:37.90,40:38.32,41:37.92,55:38.23,56:37.96,60:37.80,62:37.87,64:37.95,65:38.08,69:37.89,71:38.32,57:38.23,58:37.96,59:38.07,61:37.83,66:37.88,67:37.81,68:37.94,70:37.99}
dict_OV_map_TB1p3  = {1:37.62,3:37.39,0:37.54,2:37.57,11:37.30,12:37.60,23:0.00001,25:0.00001,47:37.83,48:37.95,49:38.01,50:37.92,38:37.85,39:37.83,40:38.17,41:37.92,55:37.93,56:37.86,60:37.82,62:38.10,64:37.80,65:38.15,69:37.90,71:37.63,57:37.79,58:37.96,59:38.16,61:37.98,66:37.80,67:37.99,68:37.86,70:37.89}
dict_OV_map_TB2    = {47:37.85,45:37.97,51:38.20,49:38.20,48:37.92,46:37.73,52:38.05,50:38.24,57:37.96,55:37.89,61:37.90,59:37.86,58:37.88,56:37.95,62:37.87,60:37.98}

SupVolt_dict = {2:41.47,3:42.48,4:43.46,5:44.47,6:45.43}
Vindac = 1.50

Voffset = 0.24 # offset of Vbias displayed by TBT DAQ w.r.t. value measured using multimeter 

df_points_new = pd.DataFrame(columns=["channel","Tileboard","BeamDate","OverVolt","ConvGain","gain","x","y","SupVolt","BiasVolt"])



chan_4mm_15um_TB2 = [51,49,52,50,61,59,62,60]
chan_2mm_15um_TB2 = [47,45,48,46,57,58,56]

        
        
for volt in df_points_chi.OverVolt.unique():
    plt.figure(figsize = (10,8))
    cnt4_15=0
    cnt2_15=0
    for chan in df_points_chi.channel.unique():
        sel  =  df_points_chi.channel == chan
        sel &=  df_points_chi.OverVolt == volt
    #                 sel &= df_chans.ped == "plus0"
        df_ana_sel = df_points_chi[sel]

        try:
            mdelay_val = df_ana_sel["gain"].max()
            mdelay_err = np.array(df_ana_sel[df_ana_sel.gain == mdelay_val].gain_err)[0]
            mdelay = np.array(df_ana_sel[df_ana_sel.gain == mdelay_val].delay)[0]


#                 if chan in chan_2mm_15um_1p22:
            if chan in chan_2mm_15um_TB2:
                if cnt2_15==0:
                    plt.errorbar(df_ana_sel["delay"],df_ana_sel["gain"],yerr=df_ana_sel["gain_err"],fmt="o",ls='-',capsize=4,c="r", label="2mm², 15µm SiPM") #,
                    plt.plot([mdelay,mdelay],[0,df_ana_sel.gain.max()],"r--")
                    cnt2_15 +=1
                else:
                    plt.errorbar(df_ana_sel["delay"],df_ana_sel["gain"],yerr=df_ana_sel["gain_err"],fmt="o",ls='-',capsize=4,c="r") #,
                    plt.plot([mdelay,mdelay],[0,df_ana_sel.gain.max()],"r--")
                    cnt2_15 +=1

#                 if chan in chan_4mm_15um_1p22:
            if chan in chan_4mm_15um_TB2:
                if cnt4_15==0:
                    plt.errorbar(df_ana_sel["delay"],df_ana_sel["gain"],yerr=df_ana_sel["gain_err"],fmt="o",ls='-',capsize=4,c="b", label="4mm², 15µm SiPM") #,
                    plt.plot([mdelay,mdelay],[0,df_ana_sel.gain.max()],"b--")
                    print(chan)
                    cnt4_15 +=1
                else:
                    plt.errorbar(df_ana_sel["delay"],df_ana_sel["gain"],yerr=df_ana_sel["gain_err"],fmt="o",ls='-',capsize=4,c="b") #,
                    plt.plot([mdelay,mdelay],[0,df_ana_sel.gain.max()],"b--")
                    print(chan)
                    cnt4_15 +=1

            df_max  = df_max.append(pd.DataFrame([[chan,12,volt,mdelay,mdelay_val,mdelay_err]],columns=df_max.columns))

        except IndexError:
            pass

    plt.grid()
    plt.legend(title='For Overvoltage:'+str(volt) + ' ConvGain:12 ')
#     plt.ylim(0,15)
#     plt.xlim(9,20)
    plt.xlabel("Delay [25/16 ns]")
    plt.ylabel("SPS gain [ADC]")

    plot_dir=base_dir+"/MIP_spectra/SPS_map/"
    if not os.path.exists(plot_dir):os.makedirs(plot_dir)
    plt.savefig(plot_dir+'SPS_OV'+str(volt)+".png")  
    print("saved as "+plot_dir+'SPS_OV'+str(volt)+".png")
    
    
    
    

df_points = df_points_chi.groupby(['channel','Tileboard','BeamDate','phase','OverVolt','ConvGain'])['gain'].mean().reset_index()
for OV in df_points.OverVolt.unique():
    s  = df_points.OverVolt == OV
    print(OV)
    # # ADC plot
    df_points_BC2_OV = df_points[s]
    df_points_BC2_OV = df_points_BC2_OV.groupby(['channel','Tileboard','BeamDate','OverVolt','ConvGain'])['gain'].max().reset_index()
    
    df_points_BC2_OV["x"] = df_points_BC2_OV.channel.map(ch_map_x)
    df_points_BC2_OV["y"] = df_points_BC2_OV.channel.map(ch_map_y)
    df_points_BC2_OV["BiasVolt"]=  df_points_BC2_OV['channel'].map(dict_OV_map_TB2)
    df_points_BC2_OV["SupVolt"] = SupVolt_dict[OV] + Voffset - df_points_BC2_OV['channel'].map(dict_OV_map_TB2) - Vindac 
    print(df_points_BC2_OV)
    
    df_points_new = df_points_new.append(df_points_BC2_OV)

    df_mp_OV = df_points_BC2_OV.pivot_table(columns='x', index='y', values='gain', fill_value=0)
    # strings_OV_15 = np.arange(8,4)

    df_ch_OV = df_points_BC2_OV.pivot_table(columns='x', index='y', values='channel', fill_value=0)
    df_ch_OV = df_ch_OV.add_prefix("ch:")

    plt.figure(figsize=(15, 8))
    sns.heatmap(df_mp_OV, vmax=6.0, vmin=3.0, cmap = "coolwarm",annot_kws={'size':14}, linewidths=0.5, annot=True, fmt=".2f")
    plt.title('SPS gain map for ConvGain='+str(df_points_BC2_OV.ConvGain.unique()[0])+', OV='+str(OV)+'V+/-0.3,'+df_points_BC2_OV.Tileboard.unique()[0])

    plot_dir=base_dir+"/MIP_spectra/SPS_map/"
    if not os.path.exists(plot_dir):os.makedirs(plot_dir)
    plt.savefig(plot_dir+'SPS_map_CGain'+str(df_points_BC2_OV.ConvGain.unique()[0])+'_'+str(df_points_BC2_OV.OverVolt.unique()[0])+'V_'+df_points_BC2_OV.Tileboard.unique()[0]+'.png')  
    print("saved as "+plot_dir + 'SPS_map_CGain'+str(df_points_BC2_OV.ConvGain.unique()[0])+'_'+str(df_points_BC2_OV.OverVolt.unique()[0])+'V_'+df_points_BC2_OV.Tileboard.unique()[0]+'.png')

    
    
    
    
    
df_sel = df_points_new.drop(columns=["x","y"])
df_points_cmplt = pd.DataFrame(columns=['channel','Tileboard','OverVolt','SupVolt','ConvGain','BiasVolt','gain'])

chan_4mm_15um_1p3 = [51,52,49,59,60,50,62,61]
chan_4mm_10um_1p3 = [40,41,64,65,66,67,68,69,70]
chan_2mm_15um_1p3 = [45,46,47,55,57,58,56,48]
chan_9mm_10um_1p3 = [ 2, 3,11,12] 

# chan_4mm_15um_TB2 = [50,59,60,62]
# chan_2mm_15um_TB2 = [48,57,58,56]
chan_4mm_15um_TB2 = [51,49,52,50,61,59,62,60]
chan_2mm_15um_TB2 = [47,45,48,46,57,58,56]

chan_15um_TB2 = np.append(chan_4mm_15um_TB2,chan_2mm_15um_TB2)

for TB in df_sel.Tileboard.unique():
    print(TB)
    for CGain in df_sel.ConvGain.unique():
        cnt2_15=0
        cnt4_15=0
        plt.figure(figsize=(10, 8))
        for channel in chan_15um_TB2:
            s1  = df_sel.channel == channel 
            s1 &= df_sel.ConvGain   == CGain
            s1 &= df_sel.Tileboard   == TB


            if len(df_sel[s1])>0:
#                 print(df_sel[s1])
                x = np.array(df_sel[s1].SupVolt)
                y = np.array(df_sel[s1].gain)
                for i,val in enumerate(x):
                    df_points_cmplt = df_points_cmplt.append(pd.DataFrame([[channel,TB,np.array(df_sel[s1].OverVolt)[i],x[i],CGain,dict_OV_map_TB2[channel],y[i]]],columns=df_points_cmplt.columns))

                if len(x)<2: 
                    x = np.append(x,[-0.5])
                    y = np.append(y,[0])
#                 print(x,np.array(df_sel[s1].OverVolt),y)
                for OVval in [2,3,4,6]:
                    if OVval not in np.array(df_sel[s1].OverVolt):
                        m,b = np.round(np.polyfit(x, y, 1),2)
                        voltoffset = SupVolt_dict[OVval] - dict_OV_map_TB2[channel] - Vindac + Voffset
                        print(channel,OVval,voltoffset)
                        df_points_cmplt = df_points_cmplt.append(pd.DataFrame([[channel,TB,OVval,voltoffset,CGain,dict_OV_map_TB2[channel],voltoffset*m+b]],columns=df_points_cmplt.columns))
                    if channel in chan_2mm_15um_TB2:
                        if cnt2_15==0:
                            plt.plot(x,y,'ro',label="2mm², 15µm SiPM")
                            cnt2_15+=1
                        else: 
                            plt.plot(x,y,'ro')

                        m,b = np.round(np.polyfit(x, y, 1),2)
                        plt.plot([-b/m,x.max()],[0,x.max()*m+b],'-r')

                    elif channel in chan_4mm_15um_TB2:
                        if cnt4_15==0:
                            plt.plot(x,y,'bo',label="4mm², 15µm SiPM")
                            cnt4_15+=1
                        else: 
                            plt.plot(x,y,'bo')

                        m,b = np.round(np.polyfit(x, y, 1),2)
                        plt.plot([-b/m,x.max()],[0,x.max()*m+b],'-b')
                
        plt.ylabel("SPS gain [ADC]")
        plt.xlabel("OverVoltage [V]")
        plt.legend(title="SPS gain for "+TB+" ConvGain:"+ str(CGain) ,fontsize='small',ncol=2)
        plt.grid()
        plt.ylim(0,23)                
        plt.xlim(-1,7)                
        plot_dir=base_dir+"/MIP_spectra/"
        if not os.path.exists(plot_dir):os.makedirs(plot_dir)
        plt.savefig(plot_dir+'SPSgainvOV_CGain'+str(CGain)+'.png')  
        print("saved as "+plot_dir+'SPSgainvOV_CGain'+str(CGain)+'.png')

        
        
        
        
### make corrected SPS dataframe

df_points_save = df_points_cmplt 

df_points_save.to_hdf(base_dir+"/root_SPS_AprTB2022_TB2_sel.h5", key = "data")
print("Saved as " + base_dir+"/root_SPS_AprTB2022_TB2_sel.h5")