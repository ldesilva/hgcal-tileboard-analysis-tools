
"""
Data summary script using template fit method.

NOTE: Can be used only after template fit has been applied. For template fitting of 6-bx events, first use 
plot_beamdata_TmpltFit_TB2.py script. 

----------------
TO EXECUTE

python3 plot_beamdata_TmpltFitSummary_TB2.py <base directory> <Tileboard version> 

<Tileboard version> = "TB2" or "TB2.1"

----------------
RETURNS

df_TestBeam.h5

    Contains a pandas DataFrame with the following columns (key="data")

                ["channel","Config","Tileboard","ConvGain","OverVolt","SupVolt","SupVolt_error",
                "ADC","ADC_error","LandauWidth","LandauWidth_error","GausWidth","GausWidth_error",
                "TotalWidth","TotalWidth_error","Ped_RMS","RMS_error","Temperature"]


    channel     = HGCROC channel number according to the tileboard floorplan. 
                     NOTE: Currently only data from second half of tileboard is used
                  Not to confuse with the raw HGCROC channels as that cordinate system is different          
    Config      = Tileboard configuration. Taken from run_map. Depends on testbeam. 
    ConvGain    = Conveyor Gain. Default is 12. Value taken from run_map. 
    OverVolt    = Over voltage according to file name (/run_OV*V)
    Temperature = Temperature at which data was taken. Taken from run_map

    SupVolt     = Over voltage according to run_map. SupVolt = Vbias - (Vindac + Vbd[from dict_OV_map]). 
                  Vbias = Vbias taken from run_map + multimeter_Vdif
                  therefore:
                     [SupVolt = Vbias(from run_map) + multimeter_Vdif - (Vindac + Vbd[from dict_OV_map])]
                  SupVolt needs to be corrected later on for the differences in temperature. 
                  (Not done in this script)

    ADC, ADC_err                 = fit parameters for MIP MPV.
    LandauWidth, LandauWidth_err = fit parameters for Landau width
    GausWidth, GausWidth_err     = fit parameters for Gaussian width
    TotalWidth, TotalWidth_err   = fit parameters for MIP width (Landau + Gaus)
    Ped_RMS, RMS_error           = fit parameters for MIP MPV


"""


import pandas as pd
import numpy as np
from glob import glob
import os

import matplotlib as mpl
import matplotlib.pyplot as plt

from langaus import LanGausFit
import langaus

import ROOT
import uproot

import math 

# set font size
font = {'size'   : 20}
mpl.rc('font', **font)


################### Variables ################################

outputHDF5file = "/df_TestBeam.h5"

root_dir = "root_files/"
run_fl = "*.root"

multimeter_Vdif = {"TB2":0.06,"TB2.1":0.24}   # Difference between multimeter and Vout of TB-tester DAQ
Vindac = 1.535                                # TB2 and above!!! voltage for inputdac=31. ( OV = Vbias - (Vindac + Vbd) )
Vindac_err = 0.010
# Vindac = 1.520                              # TB1 voltage for inputdac=31. ( OV = Vbias - (Vindac + Vbd) )
# Vbd = 38.0                                  # default breakdown voltage

# breakdown voltages
dict_OV_map    = {"TB2":{47:37.85,45:37.97,51:38.20,49:38.20,48:37.92,46:37.73,52:38.05,50:38.24,57:37.96,55:37.89,61:37.90,59:37.86,58:37.88,56:37.95,62:37.87,60:37.98},
                  "TB2.1":{47:38.00,45:38.00,51:38.00,49:38.00,48:38.00,46:38.00,52:38.00,50:38.00,57:38.00,55:38.00,61:38.00,59:38.00,58:38.00,56:38.00,62:38.00,60:38.00}}

run_map = pd.read_csv(base_dir + "/RunMaster.txt", sep = "\t")
    
# run_fl = ["root_files/run_20211029_120206.root"]
        
def main(base_dir,Tileboard):
    df_width = pd.DataFrame(columns=["channel","Config","Tileboard","ConvGain","OverVolt","SupVolt","SupVolt_error",
                                     "ADC","ADC_error","LandauWidth","LandauWidth_error","GausWidth","GausWidth_error",
                                        "TotalWidth","TotalWidth_error","Ped_RMS","RMS_error","Temperature"])

    file_lst = 0
    run_dir = base_dir + root_dir + run_fl 
    fnames = glob(run_dir)
    print("Found %i data files" % len(fnames))
    if len(fnames)>0:
        for fname in fnames:
            file = uproot.open(fname)
        #     print(file.keys())
            tree = file["data"]
        #     df_psel = tree.arrays(library="pd")         # for LCG > 101
            df_psel = tree.pandas.df()                    # for LCG =  99
    
            chan = df_psel.channel.unique()[0]
            ConvGain = df_psel.ConvGain.unique()[0]
            inpDAC = df_psel.Inputdac.unique()[0]

            run_nr = fname[fname.find(root_dir)+len(root_dir):fname.find(".root")]
            s = run_map[run_map.run_nr==run_nr]

            SupVolt = np.round(s.Vbias.values[0] + multimeter_Vdif[Tileboard] - (dict_OV_map[Tileboard][chan] + Vindac),2)
            SupVolt_err = Vindac_err

            try:
                OV = float(fname[fname.find("run_OV")+6:fname.find("V_202")])
            except ValueError:
                OV = np.round(SupVolt,0)

            config = s.Config.values[0]
            Temp = s.Temp.values[0]

            lolim = 30
            if   ConvGain <= 4 :   uplim = 300
            elif ConvGain <= 12:   uplim = 400
            elif ConvGain <= 15:   uplim = 500

            if OV >= 3.0 :  
    #             lolim = lolim*2
                uplim += uplim
            if OV >= 5.0 :  
    #             lolim += lolim*4 
                uplim += uplim*3

            nbins = int(len(df_psel)/250)
    #             nbins = 300

            if uplim >1100: uplim = 1100

            canvas1 = ROOT.TCanvas("canvas1","canvas1",800, 600)
            canvas1.SetFillColor(0)
            canvas1.SetGrid()
            canvas1.GetFrame().SetFillColor(2)
            canvas1.GetFrame().SetBorderSize(12)
    #             histLangaus = ROOT.TH1D("dataLangaus", "dataLangaus", nbins, 100,df_psel.ADC.max())
            histLangaus = ROOT.TH1D("dataLangaus", "dataLangaus", nbins, -101,uplim+10)
            histLangaus.SetAxisRange(-100,400)
            histLangaus.SetLineColor(1)
            for element in range(len(df_psel)):
                histLangaus.Fill(df_psel[df_psel.index==element].ADC)

            histLangaus.SetAxisRange(lolim,uplim,"X")
            yuplim = histLangaus.GetBinContent(histLangaus.GetMaximumBin());
            yuplim_val = histLangaus.GetXaxis().GetBinCenter(histLangaus.GetMaximumBin());
    #             print(int(lolim),uplim)
    #             print(yuplim,yuplim_val)


    #         histLangaus.SetAxisRange(-10,yuplim,"Y")

            histLangaus.GetXaxis().SetTitle("Reconstructed Amplitudes [ADC]")
            histLangaus.GetYaxis().SetTitle("No. of entries")
            histLangaus.GetXaxis().SetLabelSize(0.05);
            histLangaus.GetXaxis().SetTitleSize(0.05);
            histLangaus.GetXaxis().SetNdivisions(505);
            histLangaus.GetXaxis().SetTitleOffset(1.0);
            histLangaus.GetYaxis().SetTitleOffset(1.0);
            histLangaus.GetYaxis().SetLabelSize(0.05);
            histLangaus.GetYaxis().SetTitleSize(0.05);

            fit = LanGausFit()
    #         func = fit.fit(histLangaus,fitrange=(yuplim_val*0.5,uplim-50))
            func = fit.fit(histLangaus,fitrange=(lolim,uplim-50))

            wdth        = func.GetParameter(0)
            wdth_err    = func.GetParError(0)
            mpv         = func.GetParameter(1)
            mpv_err     = func.GetParError(1)
            norm        = func.GetParameter(2)
            norm_err    = func.GetParError(2)
            mu          = func.GetParameter(3)
            mu_err      = func.GetParError(3)
            mpv_lan     = func.GetMaximumX()
            mpv_lan_err = (mpv_err/mpv)*(mpv_lan)

            mpv_WOped  = mpv_lan - pedestal
            mpv_WOped_err  = (mpv_lan_err**2 + ped_err**2)**0.5
            snr = mpv_WOped/ped_std

            error = ((wdth)**2 + (mu)**2)**(1/2)
            error_err = np.sqrt((wdth**2 * wdth_err**2 + mu**2 * mu_err**2)/(wdth**2 + mu**2))
            chisq = func.GetChisquare()/func.GetNDF()

            ped_std = 2.0
            ped_std_err = ped_std*0.01

            df_width = df_width.append(pd.DataFrame([[int(chan),config,Tileboard,ConvGain,OV,SupVolt,SupVolt_err,
                                                      mpv_WOped,mpv_WOped_err,wdth,wdth_err,mu,mu_err,
                                                      error,error_err,ped_std,ped_std_err,Temp]],
                                                      columns=df_width.columns))

        #     In this way you find the maximum
            y_max = func.GetMaximum();
            x_max = func.GetMaximumX();


        #     //let's search for the point of half maximum, 
        #     //one will be on the left and the other one on the right
        #     //so the reason for the two call, using he lower and upper bound
        #     //of the x range for the fuction.

            fwhm_left = func.GetX(y_max/2, x_max);
            fwhm_right = func.GetX(y_max/2, x_max, df_psel.ADC.mean()+3*df_psel.ADC.std());
            fwhm = fwhm_right - fwhm_left
            print("FWHM/2.3 = " + str(fwhm/2.3))

            fwhm_left = func.GetX(9*y_max/10, x_max);
            fwhm_right = func.GetX(9*y_max/10, x_max, df_psel.ADC.mean()+3*df_psel.ADC.std());
            fwhm = fwhm_right - fwhm_left
            print("FW90%M = " + str(fwhm))

            histLangaus.SetAxisRange(-10,uplim,"X")
            histLangaus.Draw()    
            func.Draw("same")

            ROOT.gStyle.SetOptTitle(0)
            ROOT.gStyle.SetOptStat(0)
            l1=ROOT.TLegend(0.2,0.60,0.88,0.88)
            l1.SetFillStyle(0)

            l1.SetHeader("For Ch:{} at ConvGain:{} and Overvoltage:{}+/-{}".format(
                chan,ConvGain,np.round(SupVolt,2),np.round(SupVolt_err,2)),"") # option "C" allows to center the header
            l1.SetTextAlign(33)
            l1.AddEntry(0,"no. of events = {}".format(len(df_psel),""))
            l1.AddEntry(0,"fit maximum = ({}+/-{}) ADC".format(round(mpv_lan,2),round(mpv_lan_err,2)),"")
            l1.AddEntry(0,"fit width = ({}+/-{}) ADC".format(round(error,2),round(error_err,2)),"")
            l1.AddEntry(0,"Chi/NDF = {}".format(round(chisq,2)),"")
    #             l1.AddEntry(0,"RMS = {}".format(round(df_psel.ADC.std(),2)),"l")
            l1.SetMargin(0.1);
            l1.SetTextSize(0.04);
            l1.SetTextFont(42);
            l1.SetBorderSize(0);
            l1.Draw()

            print("fit maxima for Ch:{} ={}+/-{}, ConvGain:{} inputDAC:{}".format(chan,round(mpv_lan,2),round(mpv_lan_err,2),ConvGain,inpDAC))
            print("fit LandauWidth for Ch:{} ={}+/-{}, ConvGain:{} inputDAC:{}".format(chan,round(wdth,2),round(wdth_err,2),ConvGain,inpDAC))
            print("fit sigma for Ch:{} ={}+/-{}, ConvGain:{} inputDAC:{}".format(chan,round(mu,2),round(mu_err,2),ConvGain,inpDAC))
            print(len(df_psel))

            plot_dir=base_dir+"/Landau_dist_Config"+str(config)+"/OverVolt_"+ str(int(np.round(OV,0)))+"V"
            print(len(df_psel))

            if not os.path.exists(plot_dir):os.makedirs(plot_dir)
            canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,ConvGain))
            canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}.C'.format(chan,ConvGain))
            canvas1.Draw()
            canvas1.Close()
            print("saved in "+plot_dir) 

        file_lst += 1
        
    print("Found %i data files" % file_lst)

    # read dataframe
    print(df_width.head())

    # Save dataframe
    print("Saving dataframe as " + base_dir + outputHDF5file)
    df_width.to_hdf(base_dir + outputHDF5file, key = "data")

    

if __name__ == "__main__":
    if len(sys.argv) == 3:
        base_dir = sys.argv[1]
        Tileboard = sys.argv[2]
        main(base_dir,Tileboard)

    else:
        print("No argument given ("+str(len(sys.argv))+")")
        print(sys.argv)
