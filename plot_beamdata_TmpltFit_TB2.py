"""
First Data analysis script using template fit method. 
This script reads the run_map which has the information on each individual data run

Only can be used for data taken with TB-Tester DAQ system
CANNOT BE USED WITH KCU105 DAQ DATA

----------------
TO EXECUTE

python3 plot_beamdata_TmpltFit_TB2.py <base directory> <run number>

run number is the folder name within the base directory

THIS GENERATES A SINGLE ROOT FILE FOR A SINGLE RUN. NEEDS TO BE REPEATED FOR ALL CHANNELS 
TO CREATE THE FINAL DATAFRAME CONTAINING ALL DATA RUNS, EXECUTE plot_beamdata_TmpltFitSummary_TB2.py 
AFTER THIS SCRIPT FINISHES EXECUTION

----------------
RETURNS

base_dir+"/root_files/<run number>.root ---- FOR EACH RUN

    Contains a root file with the following columns (key="data")

                ["channel","ConvGain","TileboardConfig","OverVolt","SupVolt","Inputdac",
                 "bxID","ADC","ADC_err","TOT","TOA","opt_time","opt_time_err","chi2ndf","Temperature"]


    channel     = HGCROC channel number according to the tileboard floorplan. 
                     NOTE: Currently only data from second half of tileboard is used
                  Not to confuse with the raw HGCROC channels as that cordinate system is different
    ConvGain    = Conveyor Gain. Default is 12. Value taken from run_map. 
    OverVolt    = Over voltage according to file name (/run_OV*V)
    Temperature = Temperature at which data was taken. Taken from run_map

    SupVolt     = Over voltage according to run_map. SupVolt = Vbias - (Vindac + Vbd[from dict_OV_map]). 
                  Vbias = Vbias taken from run_map + multimeter_Vdif
                  therefore:
                     [SupVolt = Vbias(from run_map) + multimeter_Vdif - (Vindac + Vbd[from dict_OV_map])]
                  SupVolt needs to be corrected later on for the differences in temperature. 
                  (Not done in this script)

    ADC, ADC_err       = fit parameters for MIP MPV.
    TOA                = time of arrival in ADC
    TOT                = time over threshold in ADC

    Inputdac     = (if mentioned), inputdac of hgcroc. Default = 31
    



"""

import pandas as pd
import numpy as np
from glob import glob
import os, sys

import matplotlib as mpl
import matplotlib.pyplot as plt

from langaus import LanGausFit
import langaus
import ROOT, uproot

import math 

# set font size
font = {'size'   : 20}
mpl.rc('font', **font)

map_fname = "RunMaster.txt"

multimeter_Vdif = 0.24

def main(base_dir,run_dir):
    n_l1a  = 6 # number of n_l1a triggers expected 

    run_map = pd.read_csv(base_dir + "/" + map_fname, sep = "\t")
    s = run_map[run_map.run_nr==run_nr]
    Vbias = s.Vbias.values[0]
    chan  = s.channel.values[0]
    ConvGain = s.ConvGain.values[0]
    Temp = s.Temp.values[0]
    Config = s.Config.values[0]

    OV = np.round(Vbias + multimeter_Vdif - 39.50,0)
    SupVolt = np.round(Vbias + multimeter_Vdif - 39.50,2)
    print("Vbias:",Vbias," chan",chan," ConvGain:",ConvGain)

    #TB2
    SiPMs_2mm = [46,45,47,48,55,56,57,58]
    SiPMs_4mm = [51,52,49,50,60,62,59,61]

    nfiles = 0
    df_chans = pd.DataFrame()

    fnames = glob(base_dir + run_nr + "/beamtestrun0.root")
    print("Found %i data files" % len(fnames))

    for fname in sorted(fnames):
        file = uproot.open(fname)
        tree = file["unpacker_data"]["hgcroc"]
    #     df = tree.arrays(library="pd")         # for LCG > 101
        df = tree.pandas.df()                    # for LCG =  99
    #         Correct
        df["new_bx"] = (df.event)%n_l1a
        df["new_evt"] = (df.event)//n_l1a
        df["chan_proper"] = df["half"]*36 + df["channel"]

        nhits = df[df.adc > 200].groupby("chan_proper").size().sort_values(ascending=False)
        hit_chans = nhits.index.values[:6]
        df = df[df.chan_proper==int(chan)]   
        df_chans = df_chans.append(df)
        nfiles += 1
        
    print("hit chans:{}".format(hit_chans))
    print("selected chan:{}".format(chan))

    print(df_chans.head())
    # for both 2V OV

    evt_cut=0
    n_IndexErr = 0


    p_dif = pd.DataFrame(columns=["channel","ConvGain","TileboardConfig","OverVolt","SupVolt","Inputdac","bxID","ADC","ADC_err","TOT","TOA","opt_time","opt_time_err","chi2ndf","Temperature"])

    d_ped = {}
    d_rms = {}
    try: 
        print(df_chans.Inputdac.unique())
    except AttributeError:
        df_chans["Inputdac"] = "inputdac31"

    for ipdc in df_chans.Inputdac.unique():    
        df_sel = df_chans[df_chans.Inputdac==ipdc][:1000*n_l1a]
        df_sel = df_sel[df_sel.new_bx==n_l1a-1]
        d_ped[int(ipdc[-2:])] = df_sel.adc.median()
        d_rms[int(ipdc[-2:])] = df_sel.adc.std()
        del df_sel
    print("inputDAC medians:",d_ped)
    print("inputDAC rms:",d_rms)
    
    canvas1 = ROOT.TCanvas("canvas1","canvas1",800, 600)
    canvas1.SetFillColor(0)
    canvas1.SetGrid()
    canvas1.GetFrame().SetFillColor(2)
    canvas1.GetFrame().SetBorderSize(12)
    mg = ROOT.TMultiGraph()


    for event_no in df_chans.new_evt.unique():
#         if event_no >3000:
#             break
            
        sel = df_chans.new_evt == event_no
        x = np.float64(df_chans[sel].new_bx.values)
        y = np.float64(df_chans[sel].adc.values)

        try:
            inputdac = int(df_chans[sel].Inputdac.unique()[0][-2:])
            bxmax = y.argmax()
            ymax  = y.max()

            gr = ROOT.TGraphErrors(len(y),x,y,np.zeros(len(x)),d_rms[inputdac]*np.ones(6))
#             tf1 = ROOT.TF1("tf1","2*[0]/([3]*(2*pi)**0.5)*exp(-((x-[2])**2)/(2*([3]**2))) * 0.5*(1+ROOT::Math::erf(([4]*((x-[2])/[3]))/sqrt(2)))+[1]",0,n_l1a);
            tf1 = ROOT.TF1("tf1","2*[0]/([3]*(2*pi)**0.5)*exp(-((x-[2])**2)/(2*([3]**2))) * 0.5*(1+ROOT::Math::erf(([4]*((x-[2])/[3]))/sqrt(2)))+[1]",x.min(),x.max());
            tf1.SetParameters(ymax,d_ped[inputdac], bxmax)
    #         tf1.SetParLimits(1,d_ped[inputdac]-0.5,d_ped[inputdac]+0.5)
    #         tf1.SetParLimits(2,low_bound,high_bound)

            if chan in SiPMs_4mm:
                tf1.FixParameter(3,0.95)
                tf1.FixParameter(4,3.91)

            elif chan in SiPMs_2mm:
                tf1.FixParameter(3,0.81)
                tf1.FixParameter(4,4.26)

            gr.Fit("tf1","SQ","")

            if event_no<20:
                gr.SetMarkerColor(4);
                gr.SetMarkerStyle(21);
    #                     gr.Draw("AP")
                mg.Add(gr,"AP");

            y_plt = tf1.GetParameter(0)

            if y_plt >= 0:   
                y_plt = tf1.GetMaximum()-d_ped[inputdac]
                t_plt = tf1.GetX(tf1.GetMaximum()-0.5)
    #                     print("t_plt (max)"+str(t_plt))
            else:   
                y_plt = tf1.GetMinimum()-d_ped[inputdac]
                t_plt = tf1.GetX(tf1.GetMinimum()+0.5)
    #                     print("t_plt (min)"+str(t_plt))

            y_err = tf1.GetParError(0)
            t_err = tf1.GetParError(2)

            chi2ndf = tf1.GetChisquare()/tf1.GetNDF()

            # y_plt = popt[0] / 1.369

            TOTpr = df_chans[sel].groupby("new_bx")["tot"].max().values.mean()
            TOApr = df_chans[sel].groupby("new_bx")["toa"].median().values.mean()

            p_dif = p_dif.append(pd.DataFrame([[chan,ConvGain,Config,OV,SupVolt,inputdac,event_no,y_plt
                                                ,y_err,TOTpr,TOApr,t_plt,t_err,chi2ndf,Temp]]
                                                ,columns=p_dif.columns))

            if event_no%10000 ==0: print(event_no)
#             if event_no >1000: break
        except ValueError:
            n_IndexErr += 1
        except IndexError:
            n_IndexErr   += 1


    mg.SetTitle("Ch:"+str(chan)+";bx number;amplitude [ADC]");
    mg.Draw("AP");
    canvas1.SaveAs(base_dir+run_nr+'/Ch{}_PulseReco.png'.format(chan))
    canvas1.SaveAs(base_dir+run_nr+'/Ch{}_PulseReco.root'.format(chan))
    canvas1.Draw()

    p_dif = p_dif.astype({"channel"       :"uintc"
                        ,"ConvGain"       :"uintc"
                        ,"TileboardConfig":"uintc"
                        ,"OverVolt"       :"uintc"
                        ,"SupVolt"        :"float32"
                        ,"Inputdac"       :"uintc"
                        ,"bxID"           :"int64"
                        ,"ADC"            :"float32"
                        ,"ADC_err"        :"float32"
                        ,"TOT"            :"float32"
                        ,"TOA"            :"float32"
                        ,"opt_time"       :"float32"
                        ,"opt_time_err"   :"float32"
                        ,"chi2ndf"        :"float32"
                        ,"Temperature"    :"float32"})
    
    rdfs = {key: p_dif[key].values for key in p_dif.columns}
    rdfs = ROOT.RDF.MakeNumpyDataFrame(rdfs)
    plot_dir=base_dir+"/root_files/"
    if not os.path.exists(plot_dir):os.makedirs(plot_dir)
    rdfs.Snapshot("data",plot_dir+run_nr+".root")
    print("Saved as " +  plot_dir+run_nr+".root")

    print("no. of index errors:" + str(n_IndexErr))
    print("Pass percentage:" + str(np.round(1-n_IndexErr/event_no,2)))
    
    for gain in p_dif.ConvGain.unique():
        for inpDAC in p_dif.Inputdac.unique():
            df_psel = p_dif[p_dif.ConvGain==gain]
            df_psel = df_psel[p_dif.Inputdac==inpDAC]

            df_psel = df_psel.reset_index()
            nbins = int(len(df_psel)/20)
            print(len(df_psel))

            canvas1 = ROOT.TCanvas("canvas1","canvas1",800, 600)
            canvas1.SetFillColor(0)
            canvas1.SetGrid()
            canvas1.GetFrame().SetFillColor(2)
            canvas1.GetFrame().SetBorderSize(12)
            histLangaus = ROOT.TH1D("dataLangaus", "dataLangaus",128, -101,410)
            histLangaus.SetAxisRange(-100,400)
            histLangaus.SetLineColor(1)
            for element in range(len(df_psel)):
                histLangaus.Fill(df_psel[df_psel.index==element].ADC)

            histLangaus.Draw()    

            histLangaus.GetXaxis().SetTitle("Reconstructed Amplitudes [ADC]")
            histLangaus.GetYaxis().SetTitle("No. of entries")
            histLangaus.GetXaxis().SetLabelSize(0.05);
            histLangaus.GetXaxis().SetTitleSize(0.05);
            histLangaus.GetXaxis().SetNdivisions(505);
            histLangaus.GetXaxis().SetTitleOffset(1.0);
            histLangaus.GetYaxis().SetTitleOffset(1.0);
            histLangaus.GetYaxis().SetLabelSize(0.05);
            histLangaus.GetYaxis().SetTitleSize(0.05);

            fit = LanGausFit()
            func = fit.fit(histLangaus,fitrange=( 10,400))

            wdth        = func.GetParameter(0)
            wdth_err    = func.GetParError(0)
            mpv         = func.GetParameter(1)
            mpv_err     = func.GetParError(1)
            norm        = func.GetParameter(2)
            norm_err    = func.GetParError(2)
            mu          = func.GetParameter(3)
            mu_err      = func.GetParError(3)
            mpv_lan     = func.GetMaximumX()
            mpv_lan_err = (mpv_err/mpv)*(mpv_lan)
    #         error = ((wdth)**2 + (mu)**2 + (1.2)**2)**(1/2)
            error = ((wdth)**2 + (mu)**2)**(1/2)
            error_err = np.sqrt((wdth**2 * wdth_err**2 + mu**2 * mu_err**2)/(wdth**2 + mu**2))
            chisq = func.GetChisquare()/func.GetNDF()

        #     In this way you find the maximum
            y_max = func.GetMaximum();
            x_max = func.GetMaximumX();

        #     //let's search for the point of half maximum, 
        #     //one will be on the left and the other one on the right
        #     //so the reason for the two call, using he lower and upper bound
        #     //of the x range for the fuction.

            fwhm_left = func.GetX(y_max/2, x_max);
            fwhm_right = func.GetX(y_max/2, x_max, df_psel.ADC.mean()+3*df_psel.ADC.std());
            fwhm = fwhm_right - fwhm_left
            print("FWHM/2.3 = " + str(fwhm/2.3))

            fwhm_left = func.GetX(9*y_max/10, x_max);
            fwhm_right = func.GetX(9*y_max/10, x_max, df_psel.ADC.mean()+3*df_psel.ADC.std());
            fwhm = fwhm_right - fwhm_left
            print("FW90%M = " + str(fwhm))
            func.Draw("same")

            ROOT.gStyle.SetOptTitle(0)
            ROOT.gStyle.SetOptStat(0)
            l1=ROOT.TLegend(0.20,0.60,0.25,0.88)

            l1.SetHeader("For Ch:{} at ConvGain:{} and Overvoltage:{} {}".format(chan,gain,OV,inpDAC),"l") # option "C" allows to center the header

            l1.AddEntry(func,"no. of events = {}".format(len(df_psel),"l"))
            l1.AddEntry(func,"fit maximum = ({}+/-{}) ADC".format(round(mpv_lan,2),round(mpv_lan_err,2)),"l")
            l1.AddEntry(func,"fit width = ({}+/-{}) ADC".format(round(error,2),round(error_err,2)),"l")
            l1.AddEntry(func,"Chi/NDF = {}".format(round(chisq,2)),"l")
    #             l1.AddEntry(func,"RMS = {}".format(round(df_psel.ADC.std(),2)),"l")
            l1.SetMargin(0.1);
            l1.SetMargin(0.1);
            l1.SetTextSize(0.04);
            l1.SetTextFont(42);
            l1.SetBorderSize(0);
            l1.Draw()

            print("fit maxima for Ch:{} ={}+/-{}, gain:{} inputDAC:{}".format(chan,round(mpv_lan,2),round(mpv_lan_err,2),gain,inpDAC))
            print("fit LandauWidth for Ch:{} ={}+/-{}, gain:{} inputDAC:{}".format(chan,round(wdth,2),round(wdth_err,2),gain,inpDAC))
            print("fit sigma for Ch:{} ={}+/-{}, gain:{} inputDAC:{}".format(chan,round(mu,2),round(mu_err,2),gain,inpDAC))
            print(len(df_psel))


            plot_dir=base_dir+run_nr+"/Landau_dist"
            if not os.path.exists(plot_dir):os.makedirs(plot_dir)
            canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}_inputDAC{}.png'.format(chan,gain,inpDAC))
            canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}_inputDAC{}.C'.format(chan,gain,inpDAC))
            canvas1.Draw()
            canvas1.Close()
            print("saved in "+plot_dir) 


            
            plt.figure(figsize = (10,8))
            plt.plot(df_psel[df_psel.ConvGain==gain].bxID,df_psel[df_psel.ConvGain==gain].ADC,'o')            
            plt.legend(title="Ch:{} ConvGain:{} InputDAC:{}".format(chan,gain,inpDAC),loc=3)
            plt.xlabel("Event")
            plt.ylabel("Reconstructed Amplitude [ADC]")
            plt.ylim(0,df_psel[df_psel.ConvGain==gain].ADC.max()*1.1)
            plt.grid()
            plot_dir=base_dir+run_nr+"/EventSpread"
            if not os.path.exists(plot_dir):os.makedirs(plot_dir)
            plt.savefig(plot_dir+'/Ch{}_ConvGain{}_inputDAC{}.png'.format(chan,gain,inpDAC))  
            print("saved in "+plot_dir)
            plt.close()

            
            plt.figure(figsize = (10,8))
            plt.hist(df_psel[df_psel.ConvGain==gain].ADC,bins=np.arange(df_psel[df_psel.ConvGain==gain].ADC.min(), df_psel[df_psel.ConvGain==gain].ADC.max(), 5))
            plt.legend(title="Ch:{} ConvGain:{} InputDAC:{}".format(chan,gain,inpDAC),loc=3)
            plt.ylabel("No. of Event")
            plt.xlabel("Reconstructed Amplitude [ADC]")
            plt.grid()
            plot_dir=base_dir+run_nr+"/Landau_NOfit"
            if not os.path.exists(plot_dir):os.makedirs(plot_dir)
            plt.savefig(plot_dir+'/Ch{}_ConvGain{}_inputDAC{}.png'.format(chan,gain,inpDAC))  
            print("saved in "+plot_dir)
            plt.close()


            plt.figure(figsize = (10,8))
            plt.hist(df_psel[df_psel.ConvGain==gain].opt_time,bins=20)
            plt.legend(title="Ch:{} ConvGain:{} InputDAC:{}".format(chan,gain,inpDAC),loc=3)
            plt.ylabel("No. of Event")
            plt.xlabel("Bx at Max Amplitude [25/16 ns]")
            plt.grid()
            plot_dir=base_dir+run_nr+"/Bx_hist"
            if not os.path.exists(plot_dir):os.makedirs(plot_dir)
            plt.savefig(plot_dir+'/Ch{}_ConvGain{}_inputDAC{}.png'.format(chan,gain,inpDAC))  
            print("saved in "+plot_dir)   
            plt.close()


            plt.figure(figsize = (10,8))
            plt.plot(df_psel[df_psel.ConvGain==gain].opt_time,df_psel[df_psel.ConvGain==gain].TOA,'o')
            plt.legend(title="Ch:{} ConvGain:{} InputDAC:{}".format(chan,gain,inpDAC),loc=3)
            plt.ylabel("TOA")
            plt.xlabel("Bx at Max Amplitude [25/16 ns]")
            plt.grid()
            plot_dir=base_dir+run_nr+"/Bx_TOA"
            if not os.path.exists(plot_dir):os.makedirs(plot_dir)
            plt.savefig(plot_dir+'/Ch{}_ConvGain{}_inputDAC{}.png'.format(chan,gain,inpDAC))  
            print("saved in "+plot_dir)    
            plt.close()
            
        
if __name__ == "__main__":
    if len(sys.argv) == 3:
        base_dir = sys.argv[1]
        run_nr  = sys.argv[2]
        main(base_dir,run_nr)

    else:
        print("No argument given")
        print(sys.argv)
