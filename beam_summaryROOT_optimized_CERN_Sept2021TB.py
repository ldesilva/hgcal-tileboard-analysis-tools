import pandas as pd
import numpy as np
from glob import glob
import os

import matplotlib as mpl
import matplotlib.pyplot as plt

from langaus import LanGausFit
import langaus
import ROOT

from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.optimize import OptimizeWarning
import scipy.special as sp

import math 

import warnings
warnings.simplefilter("error", OptimizeWarning)

# set font size
font = {'size'   : 20}
mpl.rc('font', **font)

df_width = pd.DataFrame(columns=["channel","Config","ConvGain","OverVolt","ADC","ADC_error","Width","Width_error"])

base_dir = "/eos/user/l/ldesilva/CERNTB_Sep21/"
gain_fl = ["/convGain_scan_TB1p3_OV2V/","/convGain_scan_TB1p3_OV4V/","/convGain_scan_TB1p3_OV4V_FoilReverse/"]

run_fl = ["/*muon_neg*/df_summary_*.h5"]

file_lst = 0

for gain_d in gain_fl:
    if gain_d.find("_FoilReverse") > 0:
        config = "FoilReverse"
    else: 
        config = 'default'
    print()
    print()
    print("Tileboard config:{}".format(config))
    
    for run_d in run_fl:
        run_dir = base_dir + gain_d + run_d
        fnames = glob(run_dir)
        if len(fnames)>0:
            for fname in fnames:
                p_dif = pd.read_hdf(fname, key = "data")
                chan = p_dif.channel.unique()[0]

                for gain in p_dif.ConvGain.unique():
                #     plt.hist(df_psel.ADC,bins=np.arange(df_psel.ADC.min(), df_psel.ADC.max(), 11))
                    df_psel = p_dif[p_dif.ConvGain==gain]
                df_psel = df_psel.reset_index()

                strt_OV = run_dir.find("_OV")+3
                fin_OV  = run_dir.find("_OV")+4
    #             print(run_dir[strt_OV:fin_OV])
    
                OV = float(run_dir[strt_OV:fin_OV].replace('p', '.'))

                lolim = 40
                if   gain <= 4 :   uplim = 300
                elif gain <= 12:   uplim = 400
                elif gain <= 15:   uplim = 500

                if OV >= 4  :  
                    lolim = lolim*2.5
                    uplim += uplim
                if OV >= 6  :  
                    lolim += lolim*8 
                    uplim += uplim*4

                nbins = int(len(df_psel)/500)

                canvas1 = ROOT.TCanvas("canvas1","canvas1",800, 600)
                canvas1.SetFillColor(0)
                canvas1.SetGrid()
                canvas1.GetFrame().SetFillColor(2)
                canvas1.GetFrame().SetBorderSize(12)
    #             histLangaus = ROOT.TH1D("dataLangaus", "dataLangaus", nbins, 100,df_psel.ADC.max())
                histLangaus = ROOT.TH1D("dataLangaus", "dataLangaus", nbins, 0,df_psel.ADC.max())
                histLangaus.SetLineColor(1)
                for element in range(len(df_psel)):
                    histLangaus.Fill(df_psel[df_psel.index==element].ADC)

                histLangaus.Draw()

                histLangaus.SetAxisRange(lolim,uplim,"X")
                yuplim = histLangaus.GetBinContent(histLangaus.GetMaximumBin());
                yuplim_val = histLangaus.GetXaxis().GetBinCenter(histLangaus.GetMaximumBin());
    #             print(int(lolim),uplim)
    #             print(yuplim,yuplim_val)

                histLangaus.SetAxisRange(0,uplim,"X")
                histLangaus.SetAxisRange(0,yuplim,"Y")

                histLangaus.GetXaxis().SetTitle("Reconstructed Amplitudes [ADC]")
                histLangaus.GetYaxis().SetTitle("No. of entries")
                histLangaus.GetXaxis().SetLabelSize(0.05);
                histLangaus.GetXaxis().SetTitleSize(0.05);
                histLangaus.GetXaxis().SetNdivisions(505);
                histLangaus.GetXaxis().SetTitleOffset(1.0);
                histLangaus.GetYaxis().SetTitleOffset(1.0);
                histLangaus.GetYaxis().SetLabelSize(0.05);
                histLangaus.GetYaxis().SetTitleSize(0.05);

                fit = LanGausFit()
                func = fit.fit(histLangaus,fitrange=(yuplim_val*0.75,uplim-50))
            
                wdth        = func.GetParameter(0)
                wdth_err    = func.GetParError(0)
                mpv         = func.GetParameter(1)
                mpv_err     = func.GetParError(1)
                norm        = func.GetParameter(2)
                norm_err    = func.GetParError(2)
                mu          = func.GetParameter(3)
                mu_err      = func.GetParError(3)
                mpv_lan     = func.GetMaximumX()
                mpv_lan_err = (mpv_err/mpv)*(mpv_lan)
                error = ((wdth)**2 + (mu)**2 + (1.2)**2)**(1/2)
                error_err = np.sqrt((wdth**2 * wdth_err**2 + mu**2 * mu_err**2)/(wdth**2 + mu**2))
                chisq = func.GetChisquare()/func.GetNDF()

                df_width = df_width.append(pd.DataFrame([[int(chan),config,gain,OV,mpv_lan,mpv_lan_err,wdth,wdth_err]],columns=df_width.columns))

                func.Draw("same")

                ROOT.gStyle.SetOptTitle(0)
                ROOT.gStyle.SetOptStat(0)
                l1=ROOT.TLegend(0.25,0.70,0.45,0.88)
                l1.SetHeader("For Ch:{} at ConvGain:{} and Overvoltage:+{}V".format(chan,gain,OV),"l") # option "C" allows to center the header
                l1.AddEntry(func,"fit maximum = ({}+/-{}) ADC".format(round(mpv_lan,2),round(mpv_lan_err,2)),"l")
                l1.AddEntry(func,"fit LandauWidth = ({}+/-{}) ADC".format(round(wdth,2),round(wdth_err,2)),"l")
                l1.SetMargin(0.1);
                l1.SetMargin(0.1);
                l1.SetTextSize(0.04);
                l1.SetTextFont(42);
                l1.SetBorderSize(0);
                l1.Draw()
                #     canvas1.Draw()

                print("fit maxima for Ch:{} ={}+/-{}, gain:{}, OV:+{}V".format(chan,round(mpv_lan,2),round(mpv_lan_err,2),gain,OV))
                print("fit LandauWidth for Ch:{} ={}+/-{}, gain:{}, OV:+{}V".format(chan,round(wdth,2),round(wdth_err,2),gain,OV))
                print("fit GausSigma for Ch:{} ={}+/-{}, gain:{}".format(chan,round(mu,2),round(mu_err,2),gain))
                print("Chi/NDF = {}, gain:{}".format(round(chisq,2),gain))

                OV_mV = str(int(OV)) + 'V' 
                plot_dir=base_dir+"/Landau_dist_"+str(config)+"/OverVolt_"+OV_mV
                print(len(df_psel))

                if not os.path.exists(plot_dir):os.makedirs(plot_dir)
                canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,gain))
                canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}.C'.format(chan,gain))
                canvas1.Draw()
                canvas1.Close()
                print("saved in "+plot_dir) 

            file_lst += 1
    # Save dataframe
#     print("Saving dataframe as " + base_dir + gain_d + "/df_width.h5")
#     df_width.to_hdf(base_dir + gain_d + "/df_width.h5", key = "data")
print("Found %i data files" % file_lst)

print("Saving dataframe as " + base_dir + "/df_SeptCERNTestbeam2021.h5")
df_width.to_hdf(base_dir + "/df_SeptCERNTestbeam2021.h5", key = "data")
