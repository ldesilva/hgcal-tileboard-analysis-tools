"""
First Data analysis script using template fit method. 
This script reads the run_map which has the information on each individual data run

Only can be used for data taken with KCU105 DAQ system
CANNOT BE USED WITH KCU105 DAQ DATA

----------------
TO EXECUTE

python3 plot_beamdata_TmpltFit_TB1.py <base directory> <SiPM channel>

SiPM Channel is the channel number of the SiPM

THIS GENERATES A SINGLE ROOT FILE FOR A SINGLE RUN. NEEDS TO BE REPEATED FOR ALL CHANNELS 
TO CREATE THE FINAL DATAFRAME CONTAINING ALL DATA RUNS, EXECUTE plot_beamdata_TmpltFitSummary_TB1.py 
AFTER THIS SCRIPT FINISHES EXECUTION

----------------
RETURNS

base_dir+"/root_files/<run number>.root ---- FOR EACH RUN

    Contains a root file with the following columns (key="data")

                ["channel","ConvGain","TileboardConfig","OverVolt","SupVolt","Inputdac",
                 "bxID","ADC","ADC_err","TOT","TOA","opt_time","opt_time_err","chi2ndf","Temperature"]


    channel     = HGCROC channel number according to the tileboard floorplan. 
                     NOTE: Currently only data from second half of tileboard is used
                  Not to confuse with the raw HGCROC channels as that cordinate system is different
    ConvGain    = Conveyor Gain. Default is 12. Value taken from run_map. 
    OverVolt    = Over voltage according to file name (/run_OV*V)
    Temperature = Temperature at which data was taken. Taken from run_map

    SupVolt     = Over voltage according to run_map. SupVolt = Vbias - (Vindac + Vbd[from dict_OV_map]). 
                  Vbias = Vbias taken from run_map + multimeter_Vdif
                  therefore:
                     [SupVolt = Vbias(from run_map) + multimeter_Vdif - (Vindac + Vbd[from dict_OV_map])]
                  SupVolt needs to be corrected later on for the differences in temperature. 
                  (Not done in this script)

    ADC, ADC_err       = fit parameters for MIP MPV.
    TOA                = time of arrival in ADC
    TOT                = time over threshold in ADC

    Inputdac     = (if mentioned), inputdac of hgcroc. Default = 31
    


"""

import pandas as pd
import numpy as np
from glob import glob
import os, sys

import matplotlib as mpl
import matplotlib.pyplot as plt

from langaus import LanGausFit
import langaus
import ROOT

from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.optimize import OptimizeWarning
import scipy.special as sp

import math 

import warnings
warnings.simplefilter("error", OptimizeWarning)

# set font size
font = {'size'   : 20}
mpl.rc('font', **font)


def skewnormOld(x,a,h,mu):
    alpha = 2.1
    sigmag = 0.76
    
    normpdf = (1/(sigmag*np.sqrt(2*math.pi)))*np.exp(-(np.power((x-mu),2)/(2*np.power(sigmag,2))))
    normcdf = (0.5*(1+sp.erf((alpha*((x-mu)/sigmag))/(np.sqrt(2)))))
    return 2*a*normpdf*normcdf+h


def skewnormUpdate(x,a,h,mu):
    alpha = 4.09
    sigmag = 0.88
    
    normpdf = (1/(sigmag*np.sqrt(2*math.pi)))*np.exp(-(np.power((x-mu),2)/(2*np.power(sigmag,2))))
    normcdf = (0.5*(1+sp.erf((alpha*((x-mu)/sigmag))/(np.sqrt(2)))))
    return 2*a*normpdf*normcdf+h


def skewnorm2mm(x,a,h,mu):
    alpha = 4.26
    sigmag = 0.81

    normpdf = (1/(sigmag*np.sqrt(2*math.pi)))*np.exp(-(np.power((x-mu),2)/(2*np.power(sigmag,2))))
    normcdf = (0.5*(1+sp.erf((alpha*((x-mu)/sigmag))/(np.sqrt(2)))))
    return 2*a*normpdf*normcdf+h


def skewnorm4mm(x,a,h,mu):
    alpha = 3.91
    sigmag = 0.95

    normpdf = (1/(sigmag*np.sqrt(2*math.pi)))*np.exp(-(np.power((x-mu),2)/(2*np.power(sigmag,2))))
    normcdf = (0.5*(1+sp.erf((alpha*((x-mu)/sigmag))/(np.sqrt(2)))))
    return 2*a*normpdf*normcdf+h
        
    
def main(base_dir,chan):
    # for only one channel readout
    n_l1a = 6
    file=0
    dfs = pd.DataFrame()

    fnames = glob(base_dir + "/dataframe_*.h5")        
    print("Found %i data files" % len(fnames))

    #     for fname in sorted(fnames):
    for fname in sorted(fnames):
        df = pd.read_hdf(fname, key = "data")

    #         Correct
    #     df["new_bx"] = (df.event-1)%n_l1a
    #     df["new_evt"] = (df.event-1)//n_l1a

        nhits = df[df.adc > 200].groupby("channel").size().sort_values(ascending=False)
        hit_chans = nhits.index.values[:6]
        df = df[df.channel==int(chan)]   
        dfs = dfs.append(df)
        file+=1

    df_chans = dfs
    print(df_chans)

    # print(df_chans.apply(lambda row: str(row["event"]) + " " + str(row["adc"]), axis = 1))
    # df['new_bx'] = df['bx'].apply(lambda x: x+1 if x <= 53 else 'False')

    print("hit chans:{}".format(hit_chans))
    print("selected chan:{}".format(chan))

    print("ConvGain = 15")
    print(len(df_chans[df_chans.ConvGain == 15]))
    print("ConvGain = 14")
    print(len(df_chans[df_chans.ConvGain == 14]))
    print("ConvGain = 12")
    print(len(df_chans[df_chans.ConvGain == 12]))
    print("ConvGain = 10")
    print(len(df_chans[df_chans.ConvGain == 10]))
    print("ConvGain = 4")
    print(len(df_chans[df_chans.ConvGain == 4]))

    # for both 2V OV

    nnyes = {6:'-r',5:'-b'}

    n_runtimeErr = 0 
    eventID = 0
    lgth_err_min = 0
    lgth_err_max = 0
    evt_cut=0

    n_evt_bx5 = 0 
    n_evt_bx6 = 0

    bx_cnt = 0
    plt_cnt = 0

    flg = 0

    plt.figure(figsize = (10,8))
    p_dif = pd.DataFrame(columns=["channel","ConvGain","bxID","ADC","TOT","TOA","opt_time"])

    print(chan)
    for ln in range(1,len(df_chans)):
        val = np.array(df_chans["bx"][ln:ln+1])[0]-np.array(df_chans["bx"][ln-1:ln])[0]

        if val == 1:
            bx_cnt += 1
        else:
            if bx_cnt+1==n_l1a: 

                chnval = np.array(df_chans["channel"][ln-n_l1a:ln])
                adcval = np.array(df_chans["adc"][ln-n_l1a:ln])
                totval = np.array(df_chans["tot"][ln-n_l1a:ln])
                toaval = np.array(df_chans["toa"][ln-n_l1a:ln])
                df_idv = np.array(df_chans["df_id"][ln-n_l1a:ln])
                runval = np.array(df_chans["Run"][ln-n_l1a:ln])
                cgainv = np.array(df_chans["ConvGain"][ln-n_l1a:ln])

                x = np.arange(0,n_l1a)

                flg = 1
                n_evt_bx6 += 1 

            elif bx_cnt+1==n_l1a-1: 
                chnval = np.array(df_chans["channel"][ln-n_l1a+1:ln])
                adcval = np.array(df_chans["adc"][ln-n_l1a+1:ln])
                totval = np.array(df_chans["tot"][ln-n_l1a+1:ln])
                toaval = np.array(df_chans["toa"][ln-n_l1a+1:ln])
                df_idv = np.array(df_chans["df_id"][ln-n_l1a+1:ln])
                runval = np.array(df_chans["Run"][ln-n_l1a+1:ln])
                cgainv = np.array(df_chans["ConvGain"][ln-n_l1a+1:ln])

                x = np.arange(0,n_l1a-1)

                flg = 1
                n_evt_bx5 += 1


            elif bx_cnt+1>n_l1a:
                lgth_err_max += 1
                flg = 0

            else:
                lgth_err_min += 1
                flg = 0


            if flg == 1:
                y = adcval
                try:
                    gain = int(cgainv.mean())
                    try:
                        mean = y.argmax()
                        if chan < 58.5: 
                            popt,pcov = curve_fit(skewnorm4mm,x,y, p0=[y.max(),y.min(), mean])
                            y_plt = skewnorm4mm(np.arange(-0.1,n_l1a+0.1,0.01),*popt)
                        if chan > 58.5: 
                            popt,pcov = curve_fit(skewnorm2mm,x,y, p0=[y.max(),y.min(), mean])
                            y_plt = skewnorm2mm(np.arange(-0.1,n_l1a+0.1,0.01),*popt)
                        x_plt = np.arange(-0.1,n_l1a+0.1,0.01)


        #                     print(x)
        #                     print(y)

                        if plt_cnt<20 and y_plt.max()>50:
                            plt.plot(x_plt,y_plt,nnyes[bx_cnt+1])      
                            plt.plot(x,y,'.')

                        if y_plt[2] > 0:
                            y_plt = y_plt.max()-y_plt.min()
                        else:
                            y_plt = y_plt.min()-y_plt.max()
                            

                        TOTpr = totval.max()
                        TOApr = toaval.max()

                        p_dif = p_dif.append(pd.DataFrame([[chan,gain,eventID,y_plt,TOTpr,TOApr,popt[2]]],columns=p_dif.columns))

                        eventID+=1

                        if eventID%1000 ==0: print(eventID)

                    except KeyboardInterrupt:
                        n_runtimeErr += 1
                    except RuntimeError:
                        n_runtimeErr += 1
        #                 except TypeError:
        #                     n_runtimeErr += 1
        #                 except IndexError:
        #                     n_runtimeErr += 1
                    except OptimizeWarning:
                        n_runtimeErr += 1
                except KeyboardInterrupt:
                        n_runtimeErr += 1    
        #             except ValueError:
        #                 pass

            bx_cnt =0    

    plt.legend(title="Ch:{}".format(chan),loc=3)
    plt.xlabel("BX")
    plt.ylabel("Amplitude [ADC]")
    plt.savefig(base_dir+'/Ch{}_PulseReco.png'.format(chan))  
    print("saved in "+base_dir)
    plt.close()

    print(len(p_dif))

    print("no. of length errors (len(y)>6):" + str(lgth_err_max))
    print("no. of length errors (len(y)<6):" + str(lgth_err_min))
    print("no. of runtime errors:" + str(n_runtimeErr))
    # print("no. of events cut:" + str(evt_cut))
    print("no. of events passed :" + str(eventID))
    print("no. of events with 6 bxs  :" + str(n_evt_bx6))
    print("no. of events with 5 bxs  :" + str(n_evt_bx5))
    print("no. of total events (via event counts)  :" + str(n_evt_bx6+n_evt_bx5+lgth_err_min+lgth_err_max))
    print("no. of total events (via reconstruction):" + str(eventID+evt_cut+lgth_err_min+lgth_err_max+n_runtimeErr))
    print("Pass Rate:" + str(100*eventID/(eventID+evt_cut+lgth_err_min+lgth_err_max+n_runtimeErr))+" %")
    print("mean:" + str(p_dif.ADC.mean()))
    # print(p_dif) 

    print("Saving dataframe as " + base_dir + "/df_summary_ch" + str(chan) +".h5")
    p_dif.to_hdf(base_dir + "/df_summary_ch" + str(chan) +".h5", key = "data")
    
    for gain in p_dif.ConvGain.unique():
        plt.figure(figsize = (10,8))
        plt.plot(p_dif[p_dif.ConvGain==gain].bxID,p_dif[p_dif.ConvGain==gain].ADC,'o')            
        plt.legend(title="Ch:{} ConvGain:{}".format(chan,gain),loc=3)
        plt.xlabel("Event")
        plt.ylabel("Reconstructed Amplitude [ADC]")
        plt.ylim(0,p_dif[p_dif.ConvGain==gain].ADC.max()*1.1)
        plt.grid()
        plot_dir=base_dir+"/EventSpread"
        if not os.path.exists(plot_dir):os.makedirs(plot_dir)
        plt.savefig(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,gain))  
        print("saved in "+plot_dir)
        plt.close()

    for gain in p_dif.ConvGain.unique():
        plt.figure(figsize = (10,8))

        if gain==15 or gain==12:
            plt.hist(p_dif[p_dif.ConvGain==gain].ADC,bins=np.arange(p_dif[p_dif.ConvGain==gain].ADC.min(), p_dif[p_dif.ConvGain==gain].ADC.max(), 5))
        else:
            plt.hist(p_dif[p_dif.ConvGain==gain].ADC,bins=np.arange(p_dif[p_dif.ConvGain==gain].ADC.min(), p_dif[p_dif.ConvGain==gain].ADC.max(), 5))
        plt.legend(title="Ch:{} ConvGain:{}".format(chan,gain),loc=3)
        plt.ylabel("No. of Event")
        plt.xlabel("Reconstructed Amplitude [ADC]")
        plt.grid()
        plot_dir=base_dir+"/Landau_NOfit"
        if not os.path.exists(plot_dir):os.makedirs(plot_dir)
        plt.savefig(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,gain))  
        print("saved in "+plot_dir)
        plt.close()


    for gain in p_dif.ConvGain.unique():
        plt.figure(figsize = (10,8))
        plt.hist(p_dif[p_dif.ConvGain==gain].opt_time,bins=20)
        plt.legend(title="Ch:{} ConvGain:{}".format(chan,gain),loc=3)
        plt.ylabel("No. of Event")
        plt.xlabel("Bx at Max Amplitude [25/16 ns]")

        plt.grid()
        plot_dir=base_dir+"/Bx_hist"
        if not os.path.exists(plot_dir):os.makedirs(plot_dir)
        plt.savefig(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,gain))  
        print("saved in "+plot_dir)   
        plt.close()
        print(len(p_dif[p_dif.ConvGain==gain]))



    for gain in p_dif.ConvGain.unique():
        plt.figure(figsize = (10,8))
        plt.plot(p_dif[p_dif.ConvGain==gain].opt_time,p_dif[p_dif.ConvGain==gain].TOA,'o')
        plt.legend(title="Ch:{} ConvGain:{}".format(chan,gain),loc=3)
        plt.ylabel("TOA")
        plt.xlabel("Bx at Max Amplitude [25/16 ns]")
        plt.grid()
        plot_dir=base_dir+"/Bx_TOA"
        if not os.path.exists(plot_dir):os.makedirs(plot_dir)
        plt.savefig(plot_dir+'/Ch{}_ConvGain{}.png'.format(chan,gain))  
        print("saved in "+plot_dir)    
        plt.close()

    for gain in p_dif.ConvGain.unique():
        df_psel = p_dif[p_dif.ConvGain==gain]
        df_psel = df_psel.reset_index()

        strt_OV = base_dir.find("_OV")+3

        if base_dir[base_dir.find("_OV")+4] != "p":
            fin_OV  = base_dir.find("_OV")+4
        else:
            fin_OV  = base_dir.find("_OV")+6

#             print(strt_OV,fin_OV)
#             print(base_dir[strt_OV:fin_OV])
        print(base_dir[strt_OV:fin_OV].replace('p', '.'))
        OV = float(base_dir[strt_OV:fin_OV].replace('p', '.'))

        lolim = 10
        if   gain <= 4 :   uplim = 300
        elif gain <= 12:   uplim = 400
        elif gain <= 15:   uplim = 500

        if OV >= 4  :  
            lolim = lolim*4
            uplim += uplim
        if OV >= 6  :  
            lolim += lolim*8 
            uplim += uplim*2

        nbins = int(len(df_psel)/100)
        
        canvas1 = ROOT.TCanvas("canvas1","canvas1",800, 600)
        canvas1.SetFillColor(0)
        canvas1.SetGrid()
        canvas1.GetFrame().SetFillColor(2)
        canvas1.GetFrame().SetBorderSize(12)
        histLangaus = ROOT.TH1D("dataLangaus", "dataLangaus", nbins, -50,df_psel.ADC.max())
        histLangaus.SetLineColor(1)
        for element in range(len(df_psel)):
            histLangaus.Fill(df_psel[df_psel.index==element].ADC)

        histLangaus.Draw()
        
        histLangaus.SetAxisRange(lolim,uplim,"X")
        yuplim = histLangaus.GetBinContent(histLangaus.GetMaximumBin());
        print(int(lolim),uplim)
        print(yuplim)

        histLangaus.SetAxisRange(0,uplim,"X")
        histLangaus.SetAxisRange(0,yuplim,"Y")

        histLangaus.GetXaxis().SetTitle("Reconstructed Amplitudes [ADC]")
        histLangaus.GetYaxis().SetTitle("No. of entries")
        histLangaus.GetXaxis().SetLabelSize(0.05);
        histLangaus.GetXaxis().SetTitleSize(0.05);
        histLangaus.GetXaxis().SetNdivisions(505);
        histLangaus.GetXaxis().SetTitleOffset(1.0);
        histLangaus.GetYaxis().SetTitleOffset(1.0);
        histLangaus.GetYaxis().SetLabelSize(0.05);
        histLangaus.GetYaxis().SetTitleSize(0.05);


        fit = LanGausFit()
        func = fit.fit(histLangaus,fitrange=(lolim*1.1,uplim-50))

        wdth        = func.GetParameter(0)
        wdth_err    = func.GetParError(0)
        mpv         = func.GetParameter(1)
        mpv_err     = func.GetParError(1)
        norm        = func.GetParameter(2)
        norm_err    = func.GetParError(2)
        mu          = func.GetParameter(3)
        mu_err      = func.GetParError(3)
        mpv_lan     = func.GetMaximumX()
        mpv_lan_err = (mpv_err/mpv)*(mpv_lan)

        func.Draw("same")

        ROOT.gStyle.SetOptTitle(0)
        ROOT.gStyle.SetOptStat(0)
        l1=ROOT.TLegend(0.25,0.70,0.45,0.88)
        l1.SetHeader("For Ch:{} at ConvGain:{} and Overvoltage:+{}V".format(chan,gain,OV),"l") # option "C" allows to center the header
        l1.AddEntry(func,"fit maximum = ({}+/-{}) ADC".format(round(mpv_lan,2),round(mpv_lan_err,2)),"l")
        l1.AddEntry(func,"fit LandauWidth = ({}+/-{}) ADC".format(round(wdth,2),round(wdth_err,2)),"l")
        l1.SetMargin(0.1);
        l1.SetMargin(0.1);
        l1.SetTextSize(0.04);
        l1.SetTextFont(42);
        l1.SetBorderSize(0);
        l1.Draw()

        print("fit maxima for Ch:{} ={}+/-{}, gain:{}".format(chan,round(mpv_lan,2),round(mpv_lan_err,2),gain))
        print("fit width for Ch:{} ={}+/-{}, gain:{}".format(chan,round(wdth,2),round(wdth_err,2),gain))
        print(len(df_psel))
        plot_dir=base_dir+"/Landau_dist"
        if not os.path.exists(plot_dir):os.makedirs(plot_dir)
        canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}_noCut.png'.format(chan,gain))
        canvas1.SaveAs(plot_dir+'/Ch{}_ConvGain{}_noCut.C'.format(chan,gain))
        histLangaus.SaveAs(plot_dir+'/Ch{}_ConvGain{}_noCut.root'.format(chan,gain))
        canvas1.Draw()
        canvas1.Close()
        print("saved in "+plot_dir)      

        
if __name__ == "__main__":
    if len(sys.argv) == 3:
        indir = sys.argv[1]
        chan  = sys.argv[2]
        main(indir,int(chan))

    else:
        print("No argument given")
        print(sys.argv)
